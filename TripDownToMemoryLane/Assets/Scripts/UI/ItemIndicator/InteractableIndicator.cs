using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableIndicator : MonoBehaviour
{

    public Transform player;
    public Transform interactable;
    public InteractableManager itmUI;
    public bool isActive = false;
    [Header("Indicator")]
    public GameObject indicator;
    public GameObject nearestIndi;
    public float indicatorRange;
    private Vector2 uiOffset;
    private RectTransform canvas;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            if (interactable != null)
            {
                SetIndicatorPosition();
                CheckPlayerRange();
            }
            else
            {
                RemoveIndicator();
            }
        }
    }

    private void SetIndicatorPosition()
    {
        Vector2 ViewportPosition = Camera.main.WorldToViewportPoint(interactable.position);
        Vector2 proportionalPosition = new Vector2(ViewportPosition.x * canvas.sizeDelta.x, ViewportPosition.y * canvas.sizeDelta.y);

        this.GetComponent<RectTransform>().localPosition = proportionalPosition - uiOffset;
    }
    public void CheckPlayerRange()
    {
        float distance = Vector3.Distance(interactable.position, player.position);
        if (distance > indicatorRange)
        {
            itmUI.RemoveIndicatorInList(this.gameObject);
        }
    }

    public void RemoveIndicator()
    {
        itmUI.RemoveIndicatorInList(this.gameObject);
    }

    public void InteractableIsNearest(Transform plyr, Transform intrcbl)
    {
        player = plyr;
        interactable = intrcbl;
        isActive = true;
        //indicator.SetActive(true);
        indicator.SetActive(true);
        //nearestIndi.SetActive(true);
    }

    public void InteractableInRange(Transform plyr, Transform intrcbl)
    {
        //player = plyr;
        //interactable = intrcbl;
        //isActive = true;
        //indicator.SetActive(false);
        //nearestIndi.SetActive(false);
    }

    public void SetReferences(Transform plyr, Transform objTrnsfrm, RectTransform cnvs, InteractableManager prnt)
    {
        canvas = cnvs;
        this.uiOffset = new Vector2((float)canvas.sizeDelta.x / 2f, (float)canvas.sizeDelta.y / 2f);
        player = plyr;
        interactable = objTrnsfrm;
        itmUI = prnt;
        isActive = true;
        SetIndicatorPosition();
    }

    public Transform ReferencedInteractable()
    {
        return interactable;
    }

}