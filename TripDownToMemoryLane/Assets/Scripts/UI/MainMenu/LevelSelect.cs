using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelect : MonoBehaviour
{
    public bool makeAllLevelAvailable;
    public List<GameObject> lvls;

    public void CheckAvailableLevels()
    {
        if (makeAllLevelAvailable)
        {
            MakeAllLevelAvailable();
        }
        else
        {
            int lvlInd = SaveManager.instance.activeSave.currentLvl;

            for (int i = 0; i < lvlInd; i++)
            {
                lvls[i].SetActive(true);
            }
        }
    }

    public void MakeAllLevelAvailable()
    {
        makeAllLevelAvailable = true;
        for (int i = 0; i < lvls.Count; i++)
        {
            lvls[i].SetActive(true);
        }
    }

    public void DeactivateLvlSelect()
    {
        for (int i = 0; i < lvls.Count; i++)
        {
            lvls[i].SetActive(false);
        }
    }

    public void GoToSceneIndex(int i)
    {
        SceneManager.LoadScene(i);
    }
}
