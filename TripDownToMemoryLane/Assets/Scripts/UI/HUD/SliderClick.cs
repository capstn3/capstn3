using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderClick : MonoBehaviour
{
    public Sprite sliderClick;
    public Sprite sliderUnclick;

    public void ClickButton()
    {
        StopCoroutine("ClickSlideButton");
        StartCoroutine("ClickSlideButton");
    }

    IEnumerator ClickSlideButton()
    {
        gameObject.GetComponent<Image>().sprite = sliderClick;
        yield return new WaitForSeconds(0.1f);
        gameObject.GetComponent<Image>().sprite = sliderUnclick;
    }
}
