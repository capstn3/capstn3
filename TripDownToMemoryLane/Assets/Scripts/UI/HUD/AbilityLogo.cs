using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityLogo : MonoBehaviour
{
    public GameObject abilitySlider;
    public SliderClick increaseAbltySlider;
    public SliderClick decreaseAbltySlider;
    public Text abilityTxt;
    [Header("Cooldown")]
    public Image cdHud;

    public void MakeLogoActive()
    {
        abilitySlider.SetActive(true);
        gameObject.GetComponent<Image>().color = new Vector4(1, 1, 1, 1);
    }
    public void MakeLogoInactive()
    {
        abilitySlider.SetActive(false);
        gameObject.GetComponent<Image>().color = new Vector4(1, 1, 1, 0.2f);
    }

    public void ClickSliderButton(AbilitySlider sld)
    {
        switch(sld)
        {
            case AbilitySlider.Increase:
                increaseAbltySlider.ClickButton();
                break;
            case AbilitySlider.Decrease:
                decreaseAbltySlider.ClickButton();
                break;
        }
    }

    public void ChangeAbilityText(string txt)
    {
        abilityTxt.text = txt;
    }

    public void UpdateCooldown(float cdAmnt)
    {
        cdHud.fillAmount = cdAmnt;
        if (cdHud.fillAmount >= 1)
        {
            cdHud.gameObject.SetActive(false);
        }
        else
        {
            cdHud.gameObject.SetActive(true);
            if(cdHud.fillAmount < 0.3)
            {
                cdHud.color = new Color(1, 0, 0, .3f);
            }
            else if(cdHud.fillAmount < 0.6)
            {
                cdHud.color = new Color(1, .5f, 0);
            }
            else
            {
                cdHud.color = new Color(0, 1, 0);
            }
        }
    }

}
