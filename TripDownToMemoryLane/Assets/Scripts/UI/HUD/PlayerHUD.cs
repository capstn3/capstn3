using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum AbilitySlider
{
    Increase,
    Decrease
}

public class PlayerHUD : MonoBehaviour
{
    public static PlayerHUD instance;
    public GameObject wholeHUD;
    public GameObject SlidingPuzzleHUD;
    public GameObject RaylightPuzzleHUD;
    [Header("Ability Logos")]
    public AbilityLogo shutterSpeed;
    public AbilityLogo apertureControl;
    public AbilityLogo whiteBalance;
    [Header("Health")]
    public List<GameObject> healthBar;
    [Header("HiddenAttributes")]
    private bool hudIsHidden = false;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        wholeHUD.SetActive(true);
    }

    public void MakeLogoActive(PlayerAbility ablty)
    {
        if (!hudIsHidden)
        {
            switch (ablty)
            {
                case PlayerAbility.ShutterSpeed:
                    shutterSpeed.MakeLogoActive();
                    apertureControl.MakeLogoInactive();
                    whiteBalance.MakeLogoInactive();
                    break;
                case PlayerAbility.ApertureControl:
                    shutterSpeed.MakeLogoInactive();
                    apertureControl.MakeLogoActive();
                    whiteBalance.MakeLogoInactive();
                    break;
                case PlayerAbility.WhiteBalance:
                    shutterSpeed.MakeLogoInactive();
                    apertureControl.MakeLogoInactive();
                    whiteBalance.MakeLogoActive();
                    break;
                case PlayerAbility.NoEquipment:
                    shutterSpeed.MakeLogoInactive();
                    apertureControl.MakeLogoInactive();
                    whiteBalance.MakeLogoInactive();
                    break;
            }
        }
    }

    public void UpdateCooldownHUD(PlayerAbility ablty, float amnt)
    {
        switch (ablty)
        {
            case PlayerAbility.ShutterSpeed:
                shutterSpeed.UpdateCooldown(amnt);
                break;
            case PlayerAbility.ApertureControl:
                apertureControl.UpdateCooldown(amnt);
                break;
            case PlayerAbility.WhiteBalance:
                whiteBalance.UpdateCooldown(amnt);
                break;
        }
    }

    public void ChangeSliderDisplay(PlayerAbility ablty , string txt)
    {
        if (!hudIsHidden)
        {
            switch (ablty)
            {
                case PlayerAbility.ShutterSpeed:
                    shutterSpeed.ChangeAbilityText(txt);
                    break;
                case PlayerAbility.ApertureControl:
                    apertureControl.ChangeAbilityText(txt);
                    break;
                case PlayerAbility.WhiteBalance:
                    whiteBalance.ChangeAbilityText(txt);
                    break;
            }
        }
    }

    public void ShowSlidingPuzzleHUD()
    {
        HideHUD();
        SlidingPuzzleHUD.SetActive(true);
        RaylightPuzzleHUD.SetActive(false);
    }

    public void ShowRayLightPuzzleHUD(GameObject raylightContainer, Transform curRLUse)
    {
        HideHUD();
        SlidingPuzzleHUD.SetActive(false);
        RaylightPuzzleHUD.SetActive(true);
        if(raylightContainer.GetComponent<IntensityControl>()!=null)
        {
            RaylightPuzzleHUD.GetComponent<RayLightIntensityHUD>().CheckIfIntensity(true);
        }
        else
        {
            RaylightPuzzleHUD.GetComponent<RayLightIntensityHUD>().CheckIfIntensity(false);
        }
        RaylightPuzzleHUD.GetComponent<RayLightIntensityHUD>().AddRayLightIndicatorsToHUD(raylightContainer.transform.parent, curRLUse);
    }

    public void HideHUD()
    {
        wholeHUD.SetActive(false);
        hudIsHidden = true;
    }

    public void UnHideHUD()
    {
        RaylightPuzzleHUD.GetComponent<RayLightIntensityHUD>().ClearIndicators();
        wholeHUD.SetActive(true);
        SlidingPuzzleHUD.SetActive(false);
        RaylightPuzzleHUD.SetActive(false);
        hudIsHidden = false;
    }

    public void OnSliderClick(PlayerAbility ablty,AbilitySlider clck)
    {
        if (!hudIsHidden)
        {
            switch (ablty)
            {
                case PlayerAbility.ShutterSpeed:
                    shutterSpeed.ClickSliderButton(clck);
                    break;
                case PlayerAbility.ApertureControl:
                    apertureControl.ClickSliderButton(clck);
                    break;
                case PlayerAbility.WhiteBalance:
                    whiteBalance.ClickSliderButton(clck);
                    break;
            }
        }
    }

    public void UpdatePlayerHealthBar(int curHp)
    {
        int counter = 0;
        for(int h = 0; h< healthBar.Count;h++)
        {
            if((counter+1)<= curHp)
            {
                healthBar[h].SetActive(true);
            }
            else
            {
                healthBar[h].SetActive(false);
            }
            counter++;
        }
    }
    public void CheckAvailableEquipment(Loadout ldt)
    {
        if (ldt.shutterSpeedStatus.canUse)
        {
            shutterSpeed.gameObject.SetActive(true);
        }
        else
        {
            shutterSpeed.gameObject.SetActive(false);
        }
        if (ldt.apertureControlStatus.canUse)
        {
            apertureControl.gameObject.SetActive(true);
        }
        else
        {
            apertureControl.gameObject.SetActive(false);
        }
        if (ldt.whiteBalanceStatus.canUse)
        {
            whiteBalance.gameObject.SetActive(true);
        }
        else
        {
            whiteBalance.gameObject.SetActive(false);
        }
    }
}
