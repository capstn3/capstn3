using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ObjectiveSlot/Goal")]
public class SlotGoal : ScriptableObject
{
    public GoalType type;
    [Header("For Collect Objective")]
    public int requiredAmount;

}

public enum GoalType
{
    collect,
    id
}