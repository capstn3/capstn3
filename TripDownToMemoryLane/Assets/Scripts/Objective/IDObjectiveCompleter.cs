using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IDObjectiveCompleter : MonoBehaviour
{
    public int objectiveIDtoSend;
    public bool isActive = true;

    public void PublicObjectiveCheck(int idToCall)
    {
        if (isActive)
        {
            ObjectiveEvents.instance.OnObjectiveArrived(idToCall);
        }
    }

    public void PublicCollectObjectiveCheck(int collectID)
    {
        ObjectiveEvents.instance.OnCollectObjective(collectID);
    }

    private void OnPlayerArrive()
    {
        if (isActive)
        {
            ObjectiveEvents.instance.OnObjectiveArrived(objectiveIDtoSend);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            OnPlayerArrive();
        }
    }
}
