using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ObjectiveGiver : MonoBehaviour
{
    public UnityEvent onTrigger;
    public ObjectiveSlot objectiveToGive;
    private bool isActive = true;

    public void AddObjectiveToHandler()
    {
        ObjectiveHandler.instance.AddObjective(objectiveToGive);
        onTrigger.Invoke();
        isActive = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (isActive)
            {
                AddObjectiveToHandler();
            }
        }
    }
}
