using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectTypeGoal : Goal
{
    public int collectID;
    public CollectTypeGoal(string des, int cId, GoalType gt, int rqrdAmnt)
    {
        //objective = obj;
        description = des;
        collectID = cId;
        goalType = gt;
        requiredAmount = rqrdAmnt;
        Init();
    }

    public override void Init()
    {
        base.Init();
        ObjectiveEvents.instance.onCollectObjective += CheckCollectObjective;
    }

    public void CheckCollectObjective(int id)
    {
        if (!isCompleted)
        {
            if (collectID == id)
            {
                currentAmount++;
            }
            if (currentAmount >= requiredAmount)
            {
                isCompleted = true;
            }
            ObjectiveEvents.instance.CheckObjectives();
        }
    }
}
