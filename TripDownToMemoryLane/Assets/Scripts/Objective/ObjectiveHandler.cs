using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Events;

public class ObjectiveHandler : MonoBehaviour
{
    public static ObjectiveHandler instance;

    public List<Objective> curObjectives;
    public List<Objective> comObjectives;

    [Header("StartingObjectives")]
    public List<ObjectiveSlot> startObjectives;

    [Header("Sounds")]
    public AudioClip scribble;
    public AudioClip objectiveComplete;
    public AudioClip addObjective;

    [Header("Testing Variables")]
    public ObjectiveSlot slot;

    //public Objective curObj;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        ObjectiveEvents.instance.addObjective += AddObjective;
        if(startObjectives.Count>0)
        {
            PostStartingObjectives();
        }
        //curObjectives.Add(new Objective(slot));
    }

    private void PostStartingObjectives()
    {
        for(int i = 0; i< startObjectives.Count;i++)
        {
            curObjectives.Add(new Objective(startObjectives[i]));
        }
    }

    public void AddObjective(ObjectiveSlot slot)
    {
        if (!CheckIfPlayerGotObjective(slot))
        {
            Debug.Log("AddedObjs");
            curObjectives.Add(new Objective(slot));
            //this.GetComponent<AudioSource>().clip = addObjective;
           // this.GetComponent<AudioSource>().Play();
        }
    }

    private bool CheckIfPlayerGotObjective(ObjectiveSlot slot)
    {
        for (int o = 0; o < curObjectives.Count; o++)
        {
            if (slot.objID == curObjectives[o].objID)
            {
                return true;
            }
        }

        for (int o = 0; o < comObjectives.Count; o++)
        {
            if (slot.objID == comObjectives[o].objID)
            {
                return true;
            }
        }
        return false;
    }

    public void ObjectiveCompleted(Objective obj)
    {
        //this.GetComponent<AudioSource>().clip = objectiveComplete;
        //this.GetComponent<AudioSource>().Play();
        for (int o = 0; o < curObjectives.Count; o++)
        {
            if (obj == curObjectives[o])
            {
                comObjectives.Add(curObjectives[o]);
                curObjectives.RemoveAt(o);
            }
        }
    }

}
