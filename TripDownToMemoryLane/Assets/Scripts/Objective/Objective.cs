using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public class Objective
{
    public Goal goal = new Goal();
    public string ObjectiveName;
    public string ObjectiveDescription;
    public int objID;
    public ObjectiveSlot objSlot;

    public bool isCompleted;

    public Objective(ObjectiveSlot slot)
    {
        ObjectiveEvents.instance.evalObjective += CheckObjective;
        objID = slot.objID;
        ObjectiveDescription = slot.objDescription;
        objSlot = slot;
        switch (slot.goal.type)
        {
            case GoalType.id:
                goal = new ObjectiveIDGoal(slot.objDescription, objID, slot.goal.type);
                break;
            case GoalType.collect:
                goal = new CollectTypeGoal(slot.objDescription, objID, slot.goal.type, slot.goal.requiredAmount);
                break;
        }
        
        ObjectiveEvents.instance.AddedObjective();
        ObjectiveUI.instance.MakeObjectUITile(this);
        //ObjectiveDisplay.instance.AddObjectiveToObjectiveDisplay(this);
    }

    public void CheckObjective()
    {
        if(goal.isCompleted)
        {
            isCompleted = true;
        }
        if (isCompleted)
        {
            ObjectiveEvents.instance.evalObjective -= CheckObjective;
            isCompleted = true;
            ObjectiveHandler.instance.ObjectiveCompleted(this);
            ObjectiveHandler.instance.ObjectiveCompleted(this);
            //ObjectiveEvent.instance.CheckObjHandler();
            if (objSlot.chainedObjective != null)
            {
                Debug.Log("addOBj");
                ObjectiveEvents.instance.AddObjectiveSlot(objSlot.chainedObjective);
            }
        }
    }

}
