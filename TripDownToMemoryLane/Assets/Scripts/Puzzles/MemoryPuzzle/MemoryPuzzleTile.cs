using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MemoryPuzzleTile : MonoBehaviour
{
    private int orderInPuzzle;
    private MemoryPuzzle masterPuzzle;
    private Animator anim;
    private bool tileIsActive = true;
    public GameObject tileObject;
    private void Start()
    {
        anim = tileObject.GetComponent<Animator>();
    }
    private void Update()
    {
        anim.speed = TimeManager.instance.timeMultiplier;
    }
    public void SetMemoryPuzzleTile(int order, float size, MemoryPuzzle memPuz)
    {
        orderInPuzzle = order;
        tileObject.transform.localScale = new Vector3(size, size, size);
        this.GetComponent<BoxCollider>().size = new Vector3(size * .75f, size, size * .75f);
        masterPuzzle = memPuz;
    }

    private void OnStepped()
    {
        masterPuzzle.TileInteract(orderInPuzzle);
        //masterPuzzle.TileInteractMethod2(orderInPuzzle);
        tileIsActive = false;
        anim.SetBool("isNeutral", false);
        anim.SetBool("isStepped", true);
    }

    private void OnUnstepped()
    {
        //anim.SetBool("isNeutral", false);
        //anim.SetBool("isStepped", false);
        //masterPuzzle.ResetMemoryPuzzle();
        masterPuzzle.ResetMemoryPuzzleM2();
    }

    public void ResetTile()
    {
        anim.SetBool("isNeutral", false);
        anim.SetBool("isStepped", false);
        tileIsActive = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (tileIsActive)
            {
                OnStepped();
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            OnUnstepped();
        }
    }
}
