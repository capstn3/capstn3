using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MemoryPuzzle : MonoBehaviour
{
    [Header("Attributes")]
    public bool isActive = true;
    public GameObject tileObject;
    [Min(1)] public int tileNumbers;
    public float distanceBetween;
    [Min(1)] public float tileSize;
    [Min(1)] public int columns;
    [Header("HiddenAttributes")]
    private List<MemoryPuzzleTile> tiles = new List<MemoryPuzzleTile>(); //The List for all tiles
    public List<int> tilesOrder = new List<int>(); // the generated order of tiles
    private int order = 0; // current order of tiles
    public bool waitingForReset = false;
    [Header("Method2")]
    public int startingPoint = 0;
    public List<MemoryPuzzleTile> method2 = new List<MemoryPuzzleTile>();
    private void OnDrawGizmos()
    {
        float width;
        float length;
        width = (tileSize * (Mathf.Abs(tileNumbers/columns))) + (distanceBetween * (Mathf.Abs((tileNumbers)/columns)));
        length = (tileSize * columns) + (distanceBetween * (columns));
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(transform.position, new Vector3(length, tileSize, width));


        //Gizmos.color = Color.red;
        //Gizmos.DrawWireSphere(new Vector3(((length / 2))*-1, 0, (width / 2)), 1);
    }

    void Start()
    {
        GenerateTileGrid();
        GenerateTileOrder();
    }

    private void GenerateTileGrid()
    {
        float width = (tileSize * (Mathf.Abs(tileNumbers / columns))) + (distanceBetween * (Mathf.Abs((tileNumbers - 1) / columns)));
        float length = (tileSize * columns) + (distanceBetween * (columns - 1));
        Vector3 startPos = new Vector3((this.transform.position.x + (length / 2) * -1), this.transform.position.y, this.transform.position.z+(width / 2));

        int til = 0;
        int col = 0;
        for (int t = 0; t< tileNumbers;t++)
        {
            if (til == columns)
            {
                col++;
                til = 0;
            }
            float xPos = startPos.x - (((til * (tileSize + distanceBetween) * -1)) - (tileSize / 2));
            float zPos = startPos.z - (col * (tileSize + distanceBetween)) - (tileSize / 2);
            GameObject spawnedTile = Instantiate(tileObject, new Vector3(xPos, this.transform.position.y, zPos), transform.rotation, this.transform);
            spawnedTile.GetComponent<MemoryPuzzleTile>().SetMemoryPuzzleTile(t, tileSize, this);
            tiles.Add(spawnedTile.GetComponent<MemoryPuzzleTile>());
            til++;
        }
    }

    private void GenerateTileOrder()
    {
        for(int t = 0; t< tiles.Count;t++)
        {
            tilesOrder.Add(t);
        }

        for (int i = 0; i < tilesOrder.Count; i++)
        {
            int temp = tilesOrder[i];
            int randomIndex = Random.Range(i, tilesOrder.Count);
            tilesOrder[i] = tilesOrder[randomIndex];
            tilesOrder[randomIndex] = temp;
        }
    }

    #region Method2Interact
    public void TileInteractMethod2(int tlOrdr)
    {
        if(isActive)
        {
            if(method2.Count<=0)
            {
                Method2Starting(tlOrdr);
            }
            else
            {
                if(startingPoint+1>tilesOrder.Count-1)
                {
                    Debug.Log("startingtoomuch");
                    int temp = (startingPoint + 1) - (tilesOrder.Count);
                    CheckOrder(tlOrdr,temp);
                }
                else
                {
                    Debug.Log("startingisright");
                    int temp = startingPoint + 1;
                    CheckOrder(tlOrdr, temp);
                }
            }    
        }
    }

    private void Method2Starting(int tlOrdr)
    {
        for (int t = 0; t < tilesOrder.Count; t++)
        {
            if (tlOrdr == tilesOrder[t])
            {
                startingPoint = t;
                method2.Add(tiles[t]);
                break;
            }
        }
    }

    private void CheckOrder(int tlOrdr, int ord)
    {
        Debug.Log(tlOrdr + " == " + tilesOrder[ord]);
        if (tlOrdr== tilesOrder[ord])
        {
            startingPoint++;
            method2.Add(tiles[ord]);
            if(method2.Count == tiles.Count)
            {
                MemoryPuzzleComplete();
            }
        }
        else
        {
            waitingForReset = true;
        }
    }
    public void ResetMemoryPuzzleM2()
    {
        if (waitingForReset)
        {
            foreach (MemoryPuzzleTile t in tiles)
            {
                t.ResetTile();
            }
            method2.Clear();
            startingPoint = 0;
            waitingForReset = false;
        }
    }

    #endregion

    public void TileInteract(int tlOrdr)
    {
        if (isActive)
        {
            if (tlOrdr == tilesOrder[order])
            {
                order++;
                if (order == tiles.Count)
                {
                    MemoryPuzzleComplete();
                }
            }
            else
            {
                waitingForReset = true;
            }
        }
    }
    public void ResetMemoryPuzzle()
    {
        if (waitingForReset)
        {
            foreach (MemoryPuzzleTile t in tiles)
            {
                t.ResetTile();
            }
            order = 0;
            waitingForReset = false;
        }
    }

    private void MemoryPuzzleComplete()
    {
        Debug.Log("PuzzleComplete");
        isActive = false;
    }
}
