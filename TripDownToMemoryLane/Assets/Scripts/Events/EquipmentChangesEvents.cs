using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentChangesEvents : MonoBehaviour
{
    public static EquipmentChangesEvents instance;

    public delegate void OnChangeTimeScale(float scale);
    public event OnChangeTimeScale changeTimeScale;

    public delegate void OnChangeApertureSize(float scale);
    public event OnChangeApertureSize changeApertureSize; 

    public delegate void OnLightNodeChange(int id, bool check);
    public event OnLightNodeChange changeLightNodeIntensity;

    public delegate void OnChangeWhiteBalance(ColorEnum colNum);
    public event OnChangeWhiteBalance changeWhiteBalance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void ChangeTimeScale(float scale)
    {
        if (changeTimeScale != null)
        {
            changeTimeScale(scale);
        }
    }
    public void ChangeLightNode(int id, bool ck)
    {
        if (changeLightNodeIntensity != null)
        {
            changeLightNodeIntensity(id,ck);
        }
    }

    public void ChangeApertureSize(float scale)
    {
        if (changeApertureSize != null)
        {
            changeApertureSize(scale);
        }
    }

    public void ChangeWhiteBalance(ColorEnum colNum)
    {
        if (changeWhiteBalance != null)
        {
            changeWhiteBalance(colNum);
        }
    }
}
