using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveEvents : MonoBehaviour
{
    public static ObjectiveEvents instance;

    public event Action evalObjHandler;
    public event Action evalObjective;
    public event Action addedObjective;


    public delegate void ObjectiveArrived(int objectivetID);
    public event ObjectiveArrived onObjectiveArrived;

    public delegate void CollectObjective(int objectivetID);
    public event CollectObjective onCollectObjective;

    public delegate void OnAddObjective(ObjectiveSlot objslot);
    public event OnAddObjective addObjective;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }

    }
    public void AddObjectiveSlot(ObjectiveSlot slt)
    {
        if (addObjective != null)
        {
            addObjective(slt);
        }
    }

    public void AddedObjective()
    {
        if (addedObjective != null)
        {
            addedObjective();
        }
    }

    public void OnObjectiveArrived(int id)
    {
        if (onObjectiveArrived != null)
        {
            onObjectiveArrived(id);
        }
    }

    public void OnCollectObjective(int id)
    {
        Debug.Log("CollectObjectiveCalled");
        if (onCollectObjective != null)
        {
            onCollectObjective(id);
        }
    }

    public void CheckObjHandler()
    {
        if (evalObjHandler != null)
        {
            evalObjHandler();
        }
    }
    public void CheckObjectives()
    {
        if (evalObjective != null)
        {
            evalObjective();
        }
    }
}
