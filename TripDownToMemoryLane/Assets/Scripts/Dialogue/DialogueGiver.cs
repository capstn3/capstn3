using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DialogueGiver : MonoBehaviour
{
    public DialogueSet dialSet;
    public UnityEvent OnStartDialogue;
    public UnityEvent OnFinishDialogue;
    public bool dontPause;
    public bool isCinematic = false;
    private GameObject playerReference;

    public void SignalToActivate()
    {
        if (playerReference == null)
        {
            playerReference = GameObject.FindGameObjectWithTag("Player");
        }
        DialogueSystem dial = DialogueSystem.instance;
        GetComponent<BoxCollider>().enabled = false;
        dial.gameObject.SetActive(true);
        dial.ActivateDialogue(dialSet,isCinematic);
        OnStartDialogue.Invoke();
        dial.OnDialogueComplete.AddListener(OnDialogueComplete);
        //GameManager.instance.gameIsPaused = true;
        if (!dontPause)
            Time.timeScale = 0f;
    }
    public void SignalToActivateCutscene()
    {
        if (playerReference == null)
        {
            playerReference = GameObject.FindGameObjectWithTag("Player");
        }
        DialogueSystem dial = DialogueSystem.instance;
        GetComponent<BoxCollider>().enabled = false;
        dial.gameObject.SetActive(true);
        dial.ActivateDialogue(dialSet, isCinematic);
        OnStartDialogue.Invoke();
        dial.OnDialogueComplete.AddListener(OnDialogueCompleteCutsceme);
        //GameManager.instance.gameIsPaused = true;
        if (!dontPause)
            Time.timeScale = 0f;
    }
    private void OnDialogueCompleteCutsceme()
    {
        OnFinishDialogue.Invoke();
        DialogueSystem dial = DialogueSystem.instance;
        Disable();
        dial.OnDialogueComplete.RemoveListener(OnDialogueCompleteCutsceme);
        //GameManager.instance.gameIsPaused = false;
        Time.timeScale = 1f;
    }
    private void OnDialogueComplete()
    {
        OnFinishDialogue.Invoke();
        DialogueSystem dial = DialogueSystem.instance;
        Disable();
        dial.OnDialogueComplete.RemoveListener(OnDialogueComplete);
        //GameManager.instance.gameIsPaused = false;
        Time.timeScale = 1f;
        playerReference.GetComponent<PlayerMovements>().CanMove = true;
    }
    private void Disable()
    {
        DialogueSystem dial = DialogueSystem.instance;
        dial.gameObject.SetActive(false);
        this.gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            SignalToActivate();
            playerReference = other.gameObject;
            playerReference.GetComponent<PlayerMovements>().SetCantMove();
        }
    }

}
