// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/Dialogue/DialogueInput.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @DialogueInput : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @DialogueInput()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""DialogueInput"",
    ""maps"": [
        {
            ""name"": ""Proceed"",
            ""id"": ""200f0cae-3a5f-4744-a3b9-1f1f9d1d72cb"",
            ""actions"": [
                {
                    ""name"": ""Proc"",
                    ""type"": ""Button"",
                    ""id"": ""eaea79a5-9188-48b9-85d2-3e9cb5fa3e26"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""6f3ef43b-31a4-4591-98ae-3e8c57fb04a3"",
                    ""path"": ""<Keyboard>/tab"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Proc"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Proceed
        m_Proceed = asset.FindActionMap("Proceed", throwIfNotFound: true);
        m_Proceed_Proc = m_Proceed.FindAction("Proc", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Proceed
    private readonly InputActionMap m_Proceed;
    private IProceedActions m_ProceedActionsCallbackInterface;
    private readonly InputAction m_Proceed_Proc;
    public struct ProceedActions
    {
        private @DialogueInput m_Wrapper;
        public ProceedActions(@DialogueInput wrapper) { m_Wrapper = wrapper; }
        public InputAction @Proc => m_Wrapper.m_Proceed_Proc;
        public InputActionMap Get() { return m_Wrapper.m_Proceed; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(ProceedActions set) { return set.Get(); }
        public void SetCallbacks(IProceedActions instance)
        {
            if (m_Wrapper.m_ProceedActionsCallbackInterface != null)
            {
                @Proc.started -= m_Wrapper.m_ProceedActionsCallbackInterface.OnProc;
                @Proc.performed -= m_Wrapper.m_ProceedActionsCallbackInterface.OnProc;
                @Proc.canceled -= m_Wrapper.m_ProceedActionsCallbackInterface.OnProc;
            }
            m_Wrapper.m_ProceedActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Proc.started += instance.OnProc;
                @Proc.performed += instance.OnProc;
                @Proc.canceled += instance.OnProc;
            }
        }
    }
    public ProceedActions @Proceed => new ProceedActions(this);
    public interface IProceedActions
    {
        void OnProc(InputAction.CallbackContext context);
    }
}
