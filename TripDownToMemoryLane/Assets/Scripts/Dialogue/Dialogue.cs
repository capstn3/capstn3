using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[CreateAssetMenu(menuName = "Dialogue/Dialogue")]
public class Dialogue : ScriptableObject
{
    public Sprite charPortrait;
    public string sourceName;
    [TextArea(15, 20)]
    public string dialogue;

}
