using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.Events;


public class DialogueSystem : MonoBehaviour
{

    public static DialogueSystem instance = null;

    public UnityEvent OnDialogueComplete;

    private DialogueSet activeDialogue;
    private string activeDialogueText;
    [Header("DialogueAttributes")]
    public bool skipTextInstances = false;
    public float textSpeed = 0.2f;
    private bool isDialCompleted = false;

    private bool instanceTalking;

    private Text displayText;
    private Text characterTextName;
    private Image characterPortrait;
    public GameObject dialogueBackGround;

    private int index;
    private int dialogueTotal;
    [Header("Gameplay Dialogue")]
    public GameObject gameplayDial;
    public Image gamePlayPortrait;
    public Text gamePlayCharName;
    public Text gamePlayCharDial;

    [Header("Cinematic Dialogue")]
    public GameObject cinematicDial;
    public Image cinematicPortrait;
    public Text cinematicCharName;
    public Text cinematicCharDial;

    [Header("Dial")]
    public GameObject all;
    [Header("Hiddden")]
    private DialogueInput dialInput;
    private InteractionHandler interactionHandler;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }
    }
    void Start()
    {
        interactionHandler = SingletonManager.Get<InteractionHandler>();
        interactionHandler.GetDialogueInputs().Proceed.Proc.performed += _ => DialogueInput();
        dialInput = new DialogueInput();
        dialInput.Proceed.Proc.performed += _ => DialogueInput();
        //initial erase
        EraseDialogue();
        this.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void DialogueInput()
    {
        StopAllCoroutines();
        if (instanceTalking)
        {
            PostAllDialogue();
        }
        else
        {
            //Debug.Log("cont Dial");
            ContCoverstation();
        }

        if (index > dialogueTotal && !instanceTalking)
        {
            //Debug.Log("Setting False Dial");
            all.SetActive(false);
            this.gameObject.SetActive(false);
            OnDialogueComplete.Invoke();
        }
    }


    public void PostAllDialogue()
    {
        displayText.text = activeDialogue.dialogueSet[index].dialogue;
        instanceTalking = false;
        index++;
    }

    public void ActivateDialogue(DialogueSet dialogueSetID, bool isCinematic)
    {
        if(isCinematic)
        {
            gameplayDial.SetActive(false);
            cinematicDial.SetActive(true);
            dialogueBackGround.SetActive(false);
            characterPortrait = cinematicPortrait;
            characterTextName = cinematicCharName;
            displayText = cinematicCharDial;

        }
        else
        {
            cinematicDial.SetActive(false);
            gameplayDial.SetActive(true);
            dialogueBackGround.SetActive(true);
            characterPortrait = gamePlayPortrait;
            characterTextName = gamePlayCharName;
            displayText = gamePlayCharDial;
        }
        all.SetActive(true);
        isDialCompleted = false;
        index = 0;
        activeDialogue = dialogueSetID;
        dialogueTotal = activeDialogue.TotalDialogue();
        EraseDialogue();
        PrintText();
    }


    private void PrintText()
    {
        if(activeDialogue.dialogueSet[index].charPortrait!=null)
        {
            characterPortrait.gameObject.SetActive(true);
            characterPortrait.sprite = activeDialogue.dialogueSet[index].charPortrait;
        }
        else
        {
            characterPortrait.gameObject.SetActive(false);
        }
        characterTextName.text = activeDialogue.dialogueSet[index].sourceName;
        this.activeDialogueText = activeDialogue.dialogueSet[index].dialogue;

        if (skipTextInstances == true)
        {
            displayText.text = this.activeDialogueText;
        }
        else
        {
            instanceTalking = true;
            StartCoroutine(TypeWriting());
        }
    }
    private void EraseDialogue()
    {
        if (displayText != null)
        {
            displayText.text = "";
        }
        if (characterTextName != null)
        {
            characterTextName.text = "";
        }
    }

    private void ContCoverstation()
    {
        EraseDialogue();

        if (index < dialogueTotal)
        {
            PrintText();
        }
        else
        {
            index++;
        }

    }

    private void FinishConversation()
    {
        displayText.text = activeDialogue.dialogueSet[index].dialogue;
    }
    IEnumerator TypeWriting()
    {
        int cCur = 0;
        foreach (char letter in this.activeDialogueText.ToCharArray())
        {
            displayText.text += letter;
            cCur++;
            yield return new WaitForSecondsRealtime(textSpeed);
            if (cCur >= this.activeDialogueText.ToCharArray().Length)
            {
                index++;

                instanceTalking = false;
            }
        }
        if (index >= dialogueTotal)
        {

            isDialCompleted = true;
        }
    }

    public bool OnDialogueCompleted()
    {

        return isDialCompleted;
    }
}

