using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteBalancePlatform : MonoBehaviour
{
    public GameObject platform;
    [Header("WhiteBalance")]
    public ColorEnum colorCode;
    private bool isColorCoded = false;
    void Start()
    {
        EquipmentChangesEvents.instance.changeWhiteBalance += CheckColorCode;
        CheckColorCode(ColorEnum.Regular);
    }

    private void CheckColorCode(ColorEnum colNum)
    {
        if (colorCode == colNum)
        {
            isColorCoded = true;
            platform.GetComponent<Renderer>().material.SetInt("Boolean_d6da176538d04334a92d35c453b465ba", 0);
            platform.GetComponent<BoxCollider>().enabled = true;
        }
        else
        {
            isColorCoded = false;
            platform.GetComponent<Renderer>().material.SetInt("Boolean_d6da176538d04334a92d35c453b465ba", 1);
            platform.GetComponent<BoxCollider>().enabled = false;
        }
    }

    private void OnEnable()
    {
        if (GlobalVolumeManager.instance != null)
        {
            CheckColorCode(GlobalVolumeManager.instance.ReturnCurrentBalance());
        }
    }
}
