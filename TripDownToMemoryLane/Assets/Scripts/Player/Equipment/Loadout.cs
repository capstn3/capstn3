using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerAbility
{
    ShutterSpeed,
    ApertureControl,
    WhiteBalance,
    NoEquipment
}

public class Loadout : MonoBehaviour
{
    public List<Equipment> allEquipment;

    public EquipmentStatus shutterSpeedStatus;
    public EquipmentStatus apertureControlStatus;
    public EquipmentStatus whiteBalanceStatus;

    public Equipment currentEquipment = null;
    


    void Start()
    {
        SetDefaultValues();
        currentEquipment.OnEquipped();
        PlayerHUD.instance.CheckAvailableEquipment(this);
    }

    void Update()
    {
        if(currentEquipment!=null)
        {
            currentEquipment.OnUse();
        }
        //CoolDownHandler();

       
    }

  

    private void SetDefaultValues()
    {
        for (int e = 0; e < allEquipment.Count; e++)
        {
            switch (allEquipment[e].equipmentType)
            {
                case PlayerAbility.ShutterSpeed:
                    allEquipment[e].currentIndexes = allEquipment[e].defaultIndex;
                    allEquipment[e].curFailCoolDown = allEquipment[e].defFailCoolDown;
                    allEquipment[e].pLoadout = this;
                    break;
                case PlayerAbility.ApertureControl:
                    EquipmentChangesEvents.instance.ChangeApertureSize(0);
                    allEquipment[e].currentIndexes = allEquipment[e].defaultIndex;
                    allEquipment[e].curFailCoolDown = allEquipment[e].defFailCoolDown;
                    allEquipment[e].pLoadout = this;
                    break;
                case PlayerAbility.WhiteBalance:
                    allEquipment[e].currentIndexes = allEquipment[e].defaultIndex;
                    allEquipment[e].curFailCoolDown = allEquipment[e].defFailCoolDown;
                    allEquipment[e].pLoadout = this;
                    break;
            }
        }
    }

    public void ResetCurrentEquipment()
    {
        currentEquipment.ResetEquipment();
    }

    private void CoolDownHandler()
    {
        for (int e = 0; e < allEquipment.Count; e++)
        {
            allEquipment[e].EquipmentCooldown();
        }

    }

    public void ChangeEquipment(int index)
    {
        if (index > allEquipment.Count)
        {
            return;
        }
        else
        {
            if (currentEquipment == allEquipment[index-1])
            {
                return;
            }
            else
            {
                if (CanUseEquipment(allEquipment[index - 1].equipmentType))
                {
                    currentEquipment.OnUnEquipped();
                    currentEquipment = allEquipment[index - 1];
                    currentEquipment.OnEquipped();
                }
            }
        }
    }
    public void UseEquipmentIncreaseInput()
    {
        currentEquipment.OnIncrease();
    }

    public void UseEquipmentDecreaseInput()
    {
        currentEquipment.OnDecrease();
    }

    public int ReturnAbilityIndex(PlayerAbility ablty)
    {
        switch (ablty)
        {
            case PlayerAbility.ShutterSpeed:
                return shutterSpeedStatus.equipmentIndex;
            case PlayerAbility.ApertureControl:
                return apertureControlStatus.equipmentIndex;
            case PlayerAbility.WhiteBalance:
                return whiteBalanceStatus.equipmentIndex;
        }
        return -1;
    }

    public int SetAbilityIndex(PlayerAbility ablty, int curInd)
    {
        switch (ablty)
        {
            case PlayerAbility.ShutterSpeed:
                shutterSpeedStatus.equipmentIndex = curInd;
                break;
            case PlayerAbility.ApertureControl:
                apertureControlStatus.equipmentIndex = curInd;
                break;
            case PlayerAbility.WhiteBalance:
                whiteBalanceStatus.equipmentIndex = curInd;
                break;
        }
        return -1;
    }

    public bool CanUseEquipment(PlayerAbility ablty)
    {
        switch (ablty)
        {
            case PlayerAbility.ShutterSpeed:
                return shutterSpeedStatus.canUse;
            case PlayerAbility.ApertureControl:
                return apertureControlStatus.canUse;
            case PlayerAbility.WhiteBalance:
                return whiteBalanceStatus.canUse;
        }
        return false;
    }

}

[System.Serializable]
public class EquipmentStatus
{
    public bool canUse;
    public int equipmentIndex;
    public float currentCooldown;
}
