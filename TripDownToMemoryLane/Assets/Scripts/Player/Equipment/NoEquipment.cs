using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Equipment/NoEquipment")]
public class NoEquipment : Equipment
{
    public override void EquipmentCooldown()
    {
    }

    public override void GetReference(Loadout refer)
    {
    }

    public override void OnDecrease()
    {
    }

    public override void OnEquipped()
    {
        PlayerHUD.instance.MakeLogoActive(PlayerAbility.NoEquipment);
    }

    public override void OnIncrease()
    {
    }

    public override void OnUnEquipped()
    {
    }

    public override void OnUse()
    {
    }

    public override void ResetEquipment()
    {
    }

    public override void ResetToDefault()
    {
    }
}
