using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [Header("GameStatus")]
    private bool isPaused = false;
    [Header("ForCheckPoint")]
    private Vector3 respawnPoint;
    [Header("Savedata")]
    public int currentLvl;
    private int gameCurrentLvl;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        //SaveManager.instance.DeleteSaveData();
        
        if (SaveManager.instance.hasLoaded)
        {
            //gameCurrentLvl = SaveManager.instance.activeSave.currentLvl;
            if(currentLvl> SaveManager.instance.activeSave.currentLvl)
            {
                SaveManager.instance.activeSave.currentLvl = currentLvl;
                SaveManager.instance.Save();
                Debug.Log("CurrentLvlIncrease");
            }
        }
        else
        {
            SaveManager.instance.activeSave.currentLvl = currentLvl;
            SaveManager.instance.Save();
            Debug.Log("CurrentLvlNotIncrease");
        }
    }

    void Update()
    {
        
    }

    public bool GetGameIsPaused()
    {
        return isPaused;
    }

    public void SetRespawnPoint(Vector3 resPoint)
    {
        respawnPoint = resPoint;
    }

    public void PlayerIncapacitated(GameObject player)
    {
        player.SetActive(false);
        StartCoroutine("RestorePlayer",player);
    }
    IEnumerator RestorePlayer(GameObject plyr)
    {
        yield return new WaitForSeconds(3f);
        plyr.gameObject.transform.SetParent(null, true);
        plyr.transform.position = respawnPoint;
        plyr.SetActive(true);
        plyr.GetComponent<PlayerHealth>().FullHealPlayer();
    }

}
