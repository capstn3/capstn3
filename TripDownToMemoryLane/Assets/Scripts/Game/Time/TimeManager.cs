using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public static TimeManager instance;

    [Header("TimeManager")]
    [Range(0,1)]public float timeScale;
    [Min(1)] public float timeMultiplier;
    public float runtime;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else if(instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        if(!GameManager.instance.GetGameIsPaused())
        {
            runtime += Time.deltaTime * timeScale * timeMultiplier;
        }
    }
    public void ShutterSpeedEquipmentFailureStart(float changeRate)
    {
        StartCoroutine("SSEquipmentFailure", changeRate);
    }
    public void ShutterSpeedEquipmentFailureEnd()
    {
        StopCoroutine("SSEquipmentFailure");
    }
    IEnumerator SSEquipmentFailure(float rate)
    {
        int randCol = Random.Range(0, 3);
        switch (randCol)
        {
            case 0:
                TimeManager.instance.timeMultiplier = 10f;
                EquipmentChangesEvents.instance.ChangeTimeScale(10f);
                break;
            case 1:
                TimeManager.instance.timeMultiplier = 1f;
                EquipmentChangesEvents.instance.ChangeTimeScale(1f);
                break;
            case 2:
                TimeManager.instance.timeMultiplier = 0.1f;
                EquipmentChangesEvents.instance.ChangeTimeScale(0.1f);
                break;
        }
        yield return new WaitForSeconds(rate);
        StartCoroutine("SSEquipmentFailure", rate);
    }
}
