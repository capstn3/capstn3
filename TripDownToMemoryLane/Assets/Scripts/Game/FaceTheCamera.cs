using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceTheCamera : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        /*
        this.transform.position = item.transform.position;
        Vector3 targetVector = Camera.main.transform.position - this.transform.position;

        float newAngle = Mathf.Atan2(targetVector.z, targetVector.x) * Mathf.Rad2Deg;

        this.transform.rotation = Quaternion.Euler(0, -1 * newAngle, 0);
        */
        transform.forward = new Vector3(Camera.main.transform.forward.x, transform.forward.y, Camera.main.transform.forward.z);
    }
}
