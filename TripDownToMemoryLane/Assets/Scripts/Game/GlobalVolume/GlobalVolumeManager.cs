using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class GlobalVolumeManager : MonoBehaviour
{
    public static GlobalVolumeManager instance;

    public GameObject globalVolume;
    [Header("Attribute")]
    [Header("WhiteBalance")]
    [Min(1)]public float lerpSpeedWhiteBalance;
    private ColorEnum currentBalance;
    [Header("Vignette")]
    public float vignetteIntensity = 0;
    public float vignetteIntensityMax = 0.3f;
    [Header("Chromatic")]
    public float chromaticAberrationIntensity = 0;
    public float chromacticAberrationInensityMax = 1f;

    private ColorAdjustments colAdj;
    private WhiteBalance whtBlnc;
    private Vignette vignette;
    private ChromaticAberration chromaticAberration;
    


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        Volume volume = globalVolume.GetComponent<Volume>();
        WhiteBalance tmp;
        Vignette tmp1;
        ChromaticAberration tmp2;
        ColorAdjustments tmp3;
        if (volume.profile.TryGet<WhiteBalance>(out tmp))
        {
            whtBlnc = tmp;
        }

        if (volume.profile.TryGet<Vignette>(out tmp1))
        {
            vignette = tmp1;
        }

        if(volume.profile.TryGet<ChromaticAberration>(out tmp2))
        {
            chromaticAberration = tmp2;
        }

        if (volume.profile.TryGet<ColorAdjustments>(out tmp3))
        {
            colAdj = tmp3;
        }
        
    }

    void Update()
    {
       
    }

    private void SetColorAdjustmentLevel()
    {
        //colAdj.colorFilter.SetValue(new UnityEngine.Rendering.ColorParameter(darkness, true, true, true, true));
    }

    public void WhiteBalanceEquipmentFailureStart(float changeRate )
    {
        StartCoroutine("WBEquipmentFailure", changeRate);
    }
    public void ResetWhiteBalance()
    {
        StopCoroutine("ChangeWhiteBalanceProcess");
        ColorChoices colCh = new ColorChoices();
        colCh.temperature = 0;
        colCh.colorEnum = ColorEnum.Regular;
        StartCoroutine("ChangeWhiteBalanceProcess", colCh);
    }
   
    public void ResetSkills()
    {
        ResetShutterSpeed();
        ResetWhiteBalance();
    }

    public void ResetShutterSpeed()
    {
        TimeManager.instance.timeMultiplier = 1;
        chromaticAberration.intensity.SetValue(new ClampedFloatParameter(0,0,100,true));
    }
    public void WhiteBalanceEquipmentFailureEnd()
    {
        StopCoroutine("WBEquipmentFailure");
    }
    IEnumerator WBEquipmentFailure(float rate)
    {
        int randCol = Random.Range(0, 3);
        switch(randCol)
        {
            case 0:
                EquipmentChangesEvents.instance.ChangeWhiteBalance(ColorEnum.Cool);
                whtBlnc.temperature.SetValue(new UnityEngine.Rendering.ClampedFloatParameter(-100, -100, 100, true));
                break;
            case 1:
                EquipmentChangesEvents.instance.ChangeWhiteBalance(ColorEnum.Regular);
                whtBlnc.temperature.SetValue(new UnityEngine.Rendering.ClampedFloatParameter(0, -100, 100, true));
                break;
            case 2:
                EquipmentChangesEvents.instance.ChangeWhiteBalance(ColorEnum.Warm);
                whtBlnc.temperature.SetValue(new UnityEngine.Rendering.ClampedFloatParameter(100, -100, 100, true));
                break;
        }
        yield return new WaitForSeconds(rate);
        StartCoroutine("WBEquipmentFailure", rate);
    }

    public ColorEnum ReturnCurrentBalance()
    {
        return currentBalance;
    }

    public void ChangeWhiteBalance(ColorChoices col)
    {
        StopCoroutine("ChangeWhiteBalanceProcess");
        StartCoroutine("ChangeWhiteBalanceProcess", col);
    }

    IEnumerator ChangeWhiteBalanceProcess(ColorChoices clch)
    {
        yield return new WaitForSeconds(Time.deltaTime);
        if (whtBlnc.temperature.value >= clch.temperature-5&& whtBlnc.temperature.value <= clch.temperature + 5)
        {
            EquipmentChangesEvents.instance.ChangeWhiteBalance(clch.colorEnum);
            currentBalance = clch.colorEnum;
        }
        else
        {
            whtBlnc.temperature.SetValue(new UnityEngine.Rendering.ClampedFloatParameter(Mathf.Lerp(whtBlnc.temperature.value, clch.temperature, Time.deltaTime* lerpSpeedWhiteBalance), -100, 100, true));
            StartCoroutine("ChangeWhiteBalanceProcess", clch);
        }    
    }

    public void ActivateShutterSpeedEffect()
    {
        
        StopCoroutine(DecreaseVignetteIntensityEffect());
        StopCoroutine(DecreaseChromacticAberrationIntensityEffect());
        StartCoroutine(IncreaseVignetteIntensityEffect());
        StartCoroutine(IncreaseChromacticAberrationIntensityEffect());


    }

    public void DeactivateShutterSpeedEffect()
    {
        
        StopCoroutine(IncreaseVignetteIntensityEffect());
        StopCoroutine(IncreaseChromacticAberrationIntensityEffect());
        StartCoroutine(DecreaseVignetteIntensityEffect());
        StartCoroutine(DecreaseChromacticAberrationIntensityEffect());
    }


    private IEnumerator IncreaseVignetteIntensityEffect()
    {
        
        while (vignetteIntensity < vignetteIntensityMax)
        {
            yield return null;

            Debug.Log("Intensity");
            vignetteIntensity += Time.deltaTime;
            vignette.intensity.SetValue(new ClampedFloatParameter(vignetteIntensity, 0f, 1f));
        }

        
    }

    private IEnumerator IncreaseChromacticAberrationIntensityEffect()
    {
        
        while (chromaticAberrationIntensity < chromacticAberrationInensityMax)
        {
            
            yield return null;
            chromaticAberrationIntensity += Time.deltaTime;
            chromaticAberration.intensity.SetValue(new ClampedFloatParameter(chromaticAberrationIntensity, 0f, 1f));

        }


    }

    private IEnumerator DecreaseVignetteIntensityEffect()
    {
        StopCoroutine(IncreaseVignetteIntensityEffect());
        while (vignetteIntensity >= 0)
        {

            yield return null;

            Debug.Log("Intensity");
            vignetteIntensity -= Time.deltaTime;
            vignette.intensity.SetValue(new ClampedFloatParameter(vignetteIntensity, 0f, 1f));
        }


    }

    private IEnumerator DecreaseChromacticAberrationIntensityEffect()
    {
        StopCoroutine(IncreaseChromacticAberrationIntensityEffect());
        while (chromaticAberrationIntensity >= 0)
        {
            
            yield return null;
            chromaticAberrationIntensity -= Time.deltaTime;
            chromaticAberration.intensity.SetValue(new ClampedFloatParameter(chromaticAberrationIntensity, 0f, 1f));
        }


    }






}



