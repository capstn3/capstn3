using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeHazard : MonoBehaviour
{
    public enum SpikePosition
    {
        left,
        right,
        middle
    }
    [Header("Attribute")]
    public bool isSpikeMoving = false;
    public SpikePosition spikePos;
    public float distance;
    public float speed;
    [Range(-1,1)]public float moveDirection;
    public float bumpForce;
    public GameObject spikes;
    [Header("HiddenAttributes")]
    private Vector3 originalPosition;
    private float timeScaleMultiplier = 1;
    [Header("NeededAttributes")]
    public List<GameObject> spikesVar;
    public List<Transform> spikePositions;
    public int spikesToSpawn;
    [Header("WhiteBalance")]
    public ColorEnum colorCode;
    public bool affectedByWhitebalance = false;
    private bool isColorCoded = false;

    private void OnDrawGizmos()
    {
        Vector3 direction = this.transform.position;
        switch (spikePos)
        {
            case SpikePosition.left:
                Gizmos.color = Color.green;
                Gizmos.DrawLine(this.transform.position, this.transform.position + (this.transform.right * distance*-1));
                break;
            case SpikePosition.right:
                Gizmos.color = Color.green;
                Gizmos.DrawLine(this.transform.position, this.transform.position + (this.transform.right * distance ));
                break;
            case SpikePosition.middle:
                Gizmos.color = Color.green;
                //Gizmos.DrawLine(this.transform.right * (distance / 2), this.transform.right * (distance / 2) * -1);
                Gizmos.DrawLine(this.transform.position + (this.transform.right * (distance / 2)), this.transform.position + (this.transform.right * (distance / 2) * -1));
                break;
        }

    }
    void Start()
    {
        EquipmentChangesEvents.instance.changeWhiteBalance += CheckColorCode;
        EquipmentChangesEvents.instance.changeTimeScale += ChangeSpeedBaseOnTimeScale;
        originalPosition = spikes.transform.localPosition;
        SpawnRandomSpikes();
        CheckColorCode(ColorEnum.Regular);
        if(!isSpikeMoving)
        {
            foreach(Transform spk in spikes.transform)
            {
                if(spk.GetComponent<Animator>()!=null)
                {
                    spk.GetComponent<Animator>().enabled = false;
                }
            }
        }
    }

    void Update()
    {
        if(isSpikeMoving)
        {
            SpikeMove();
        }
    }

    private void SpawnRandomSpikes()
    {
        if(spikesToSpawn<=0||spikesToSpawn>spikePositions.Count)
        {
            return;
        }
        else if(spikesToSpawn ==1)
        {
            int spkVr = Random.Range(0, spikesVar.Count);
            int spkPstn = Random.Range(0, spikePositions.Count - 1);
            GameObject spk = Instantiate(spikesVar[spkVr], spikePositions[spkPstn].position, spikes.transform.rotation, spikes.transform);
            spk.GetComponent<Spike>().sHazard = this;
            switch (spkVr)
            {
                case 1:
                    break;
                case 0:
                    spk.GetComponent<Animator>().Play("Base Layer.Female_Walk_Foward",0,0.25f);
                    break;
            }
        }
        else
        {
            for(int s = 0;s<spikesToSpawn;s++)
            {
                int spkVr = Random.Range(0, spikesVar.Count);
                int spkPstn = Random.Range(0, spikePositions.Count - 1);
                GameObject spk = Instantiate(spikesVar[spkVr], spikePositions[spkPstn].position, spikes.transform.rotation, spikes.transform);
                spk.GetComponent<Spike>().sHazard = this;
                spikePositions.RemoveAt(spkPstn);
                switch (spikesVar[spkVr].name)
                {
                    case "MaleSpike":
                        if (!isSpikeMoving)
                        {
                            spk.GetComponent<Animator>().enabled = false;
                        }
                        else
                        {
                            float aRand = Random.Range(0f, 1f);
                            spk.GetComponent<Animator>().Play("Male_Walk_Foward", 0, aRand);
                        }
                        break;
                    case "FemaleSpike":
                        if (!isSpikeMoving)
                        {
                            spk.GetComponent<Animator>().enabled = false;
                        }
                        else
                        {
                            float aRand2 = Random.Range(0f, 1f);
                            spk.GetComponent<Animator>().Play("Female_Walk_Foward", 0, aRand2);
                        }
                        break;
                }
            }
        }
    }
    private void SpikeMove()
    {
        spikes.transform.Translate(Vector3.right * Time.deltaTime * moveDirection * speed * timeScaleMultiplier);
        CheckSpikeDistance();
        CheckSpikeDirection();
    }
    private void CheckSpikeDirection()
    {
        if(moveDirection==1)
        {
            foreach (Transform t in spikes.transform)
            {
                t.localEulerAngles = new Vector3(0, 90, 0);
            }
        }
        else if(moveDirection == -1)
        {
            foreach (Transform t in spikes.transform)
            {
                t.localEulerAngles = new Vector3(0, -90, 0);
            }
        }
    }
    private void CheckSpikeDistance()
    {
        switch (spikePos)
        {
            case SpikePosition.left:
                if (spikes.transform.localPosition.x >= originalPosition.x)
                {
                    moveDirection = -1;
                }
                else if (spikes.transform.localPosition.x <= originalPosition.x - distance)
                {
                    moveDirection = 1;
                }
                break;
            case SpikePosition.right:
                if (spikes.transform.localPosition.x >= originalPosition.x + distance)
                {
                    moveDirection = -1;
                }
                else if (spikes.transform.localPosition.x <= originalPosition.x)
                {
                    moveDirection = 1;
                }
                break;
            case SpikePosition.middle:
                if (spikes.transform.localPosition.x >= originalPosition.x + (distance / 2))
                {
                    moveDirection = -1;
                }
                else if (spikes.transform.localPosition.x <= originalPosition.x - (distance / 2))
                {
                    moveDirection = 1;
                }
                break;
        }
    }
    public void Bumped(Rigidbody rb, Vector3 pos)
    {
        Debug.Log("Bumped : " + rb.gameObject.name);
        if (!rb.gameObject.GetComponent<PlayerHealth>().playerIsInvulnerable)
        {
            Vector3 dir = rb.position - pos;
            Vector3 push = new Vector3(dir.x, 0, dir.z);
            rb.velocity = push * bumpForce;
            rb.gameObject.GetComponent<PlayerMovements>().BumpedToHazard();
            rb.gameObject.GetComponent<PlayerHealth>().PlayerTakeDamage(1);
        }
    }
    private void ChangeSpeedBaseOnTimeScale(float timeMultiplier)
    {
        timeScaleMultiplier = timeMultiplier;
        foreach (Transform t in spikes.transform)
        {
            if (t.GetComponent<Animator>() != null)
            {
                t.GetComponent<Animator>().SetFloat("speedMultiplier", 1 * timeMultiplier);
            }
        }
    }
    private void CheckColorCode(ColorEnum colNum)
    {
        if (affectedByWhitebalance)
        {
            if (colorCode == colNum)
            {
                spikes.SetActive(true);
            }
            else
            {
                spikes.SetActive(false);
            }
        }
    }
    private void OnEnable()
    {
        ChangeSpeedBaseOnTimeScale(TimeManager.instance.timeMultiplier);
    }
}
