using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardObjects : MonoBehaviour
{
    private Rigidbody rb;
    private float gravity = -9.81f;
    [Header("Attributes")]
    public float lethalSpeed;
    public float duration;
    public float respawnDuration;
    [Header("HiddenAttributes")]
    private Vector3 originalPos;
    private Vector3 originalRot;
    private Vector3 originalScal;
    private float defDuration;
    private bool isActive = true;
    private GameObject parent;
    // Start is called before the first frame update
    void Start()
    {
        parent = this.transform.parent.gameObject;
        rb = GetComponent<Rigidbody>();
        originalPos = transform.position;
        originalRot = transform.rotation.eulerAngles;
        originalScal = transform.localScale;
        defDuration = duration;
    }

    // Update is called once per frame
    void Update()
    {
        rb.AddForce(new Vector3(0, 1.0f, 0) * rb.mass * gravity * Time.timeScale);
        if (rb.velocity.magnitude<lethalSpeed)
        {
            duration -= Time.deltaTime;
            if(duration<=0&&isActive)
            {
                isActive = false;
                StartCoroutine("DisappearObject");
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Debug.Log(rb.velocity.magnitude + ">" + lethalSpeed);
            if (rb.velocity.magnitude > lethalSpeed&&isActive)
            {
                collision.gameObject.GetComponent<PlayerHealth>().SquashPlayer();
            }
        }
    }

    IEnumerator DisappearObject()
    {
        
        yield return new WaitForSeconds(Time.deltaTime);
        if(transform.localScale.magnitude > .5f)
        {
            transform.localScale = new Vector3(Mathf.Lerp(transform.localScale.x, .01f, Time.deltaTime), Mathf.Lerp(transform.localScale.y, .01f, Time.deltaTime), Mathf.Lerp(transform.localScale.z, .01f, Time.deltaTime));
            StartCoroutine("DisappearObject");
        }
        else
        {
            StartCoroutine("ResetObject");
        }
    }

    IEnumerator ResetObject()
    {
        yield return new WaitForSeconds(respawnDuration);
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        this.transform.position = originalPos;
        this.transform.eulerAngles = originalRot;
        this.transform.localScale = originalScal;
        duration = defDuration;
        isActive = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "DeathPit")
        {
            StartCoroutine("DisappearObject");
        }
    }

}
