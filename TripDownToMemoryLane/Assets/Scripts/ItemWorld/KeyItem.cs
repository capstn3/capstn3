using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class KeyItem : Item
{
    public int keyID;
}
