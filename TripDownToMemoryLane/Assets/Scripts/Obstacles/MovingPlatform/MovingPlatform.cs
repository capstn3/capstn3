using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public enum PlatformMode
    {
        vertical,
        horizontal,
        horizontalV2
    }
    [Header("Attributes")]
    public PlatformMode platformMode;
    public Transform platform;
    public float distanceTravel;
    public float speed;
    [Range(-1,1)]public int moveDirection = 1;
    [Header("WhiteBalance")]
    public ColorEnum colorCode;
    public bool affectedByWhitebalance = false;
    private bool isColorCoded = false;
    [Header("HiddenAttributes")]
    //private int moveDirection = 1;
    private float timeScaleMultiplier = 1;
    private Vector3 originalPosition;
    [Header("EmissiveProperty")]
    public Material warm;
    public Material cool;
    public Material regular;
    private void OnDrawGizmos()
    {
        switch(platformMode)
        {
            case PlatformMode.horizontal:
                Gizmos.color = Color.green;
                Gizmos.DrawLine(this.transform.position + (Vector3.right * (distanceTravel / 2) * -1), this.transform.position + (Vector3.right * (distanceTravel / 2)));
                break;
            case PlatformMode.vertical:
                Gizmos.color = Color.green;
                Gizmos.DrawLine(this.transform.position + (Vector3.up * (distanceTravel / 2) * -1), this.transform.position + (Vector3.up * (distanceTravel / 2)));
                break;
            case PlatformMode.horizontalV2:
                Gizmos.color = Color.green;
                Gizmos.DrawLine(this.transform.position + (Vector3.forward * (distanceTravel / 2) * -1), this.transform.position + (Vector3.forward * (distanceTravel / 2)));
                break;
        }

    }

    void Start()
    {
        EquipmentChangesEvents.instance.changeTimeScale += ChangeSpeedBaseOnTimeScale;
        EquipmentChangesEvents.instance.changeWhiteBalance += CheckColorCode;

        CheckColorCode(ColorEnum.Regular);
        originalPosition = platform.transform.position;
        if(affectedByWhitebalance)
        {
            ChangeEmissiveMaterial();
        }
    }

    void Update()
    {
        if(affectedByWhitebalance)
        {
            if(isColorCoded)
            {
                MovePlatform();
            }
        }
        else
        {
            MovePlatform();
        }
    }

    private void MovePlatform()
    {
        switch (platformMode)
        {
            case PlatformMode.horizontal:
                platform.transform.Translate(Vector3.right * Time.deltaTime * moveDirection * speed * timeScaleMultiplier);
                CheckPlatformDistance();
                break;
            case PlatformMode.vertical:
                platform.transform.Translate(Vector3.up * Time.deltaTime * moveDirection * speed * timeScaleMultiplier);
                CheckPlatformDistance();
                break;
            case PlatformMode.horizontalV2:
                platform.transform.Translate(Vector3.forward * Time.deltaTime * moveDirection * speed * timeScaleMultiplier);
                CheckPlatformDistance();
                break;
        }
    }

    private void CheckPlatformDistance()
    {
        switch (platformMode)
        {
            case PlatformMode.horizontal:
                if (platform.transform.position.x >= originalPosition.x + (distanceTravel / 2)) 
                {
                    moveDirection = -1;
                }
                else if(platform.transform.position.x <= originalPosition.x - (distanceTravel / 2))
                {
                    moveDirection = 1;
                }
                break;
            case PlatformMode.vertical:
                if (platform.transform.position.y >= originalPosition.y + (distanceTravel / 2))
                {
                    moveDirection = -1;
                }
                else if (platform.transform.position.y <= originalPosition.y - (distanceTravel / 2))
                {
                    moveDirection = 1;
                }
                break;
            case PlatformMode.horizontalV2:
                if (platform.transform.position.z >= originalPosition.z + (distanceTravel / 2))
                {
                    moveDirection = -1;
                }
                else if (platform.transform.position.z <= originalPosition.z - (distanceTravel / 2))
                {
                    moveDirection = 1;
                }
                break;
        }
    }

    private void ChangeSpeedBaseOnTimeScale(float timeScale)
    {
        timeScaleMultiplier = timeScale;
    }

    private void CheckColorCode(ColorEnum colNum)
    {
        if(colorCode == colNum)
        {
            isColorCoded = true;
        }
        else
        {
            isColorCoded = false;
        }
    }
    private void ChangeEmissiveMaterial()
    {
        switch(colorCode)
        {
            case ColorEnum.Cool:
                List<Material> tempMat = new List<Material>(platform.gameObject.GetComponent<MeshRenderer>().materials);
                tempMat[1] = cool;
                platform.gameObject.GetComponent<MeshRenderer>().materials = tempMat.ToArray();
                break;
            case ColorEnum.Warm:
                List<Material> tempMat2 = new List<Material>(platform.gameObject.GetComponent<MeshRenderer>().materials);
                tempMat2[1] = warm;
                platform.gameObject.GetComponent<MeshRenderer>().materials = tempMat2.ToArray();
                break;
            case ColorEnum.Regular:
                List<Material> tempMat3 = new List<Material>(platform.gameObject.GetComponent<MeshRenderer>().materials);
                tempMat3[1] = regular;
                platform.gameObject.GetComponent<MeshRenderer>().materials = tempMat3.ToArray();
                break;
        }
    }

    public void StopPlatform()
    {
        speed = 0;
    }
}

