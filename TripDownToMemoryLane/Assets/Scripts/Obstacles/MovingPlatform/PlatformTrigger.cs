using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformTrigger : MonoBehaviour
{
    private bool isOkayToParent = true;

    /*
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player"&&isOkayToParent)
        {
            other.gameObject.transform.parent = this.transform;
        }
    }*/

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.transform.SetParent(null, true);
            isOkayToParent = false;
            StartCoroutine("ParentCooldown");
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player" && isOkayToParent)
        {
            if (this.transform.position.y < other.gameObject.transform.position.y)
            {
                other.gameObject.transform.parent = this.transform;
            }
        }
    }
    IEnumerator ParentCooldown()
    {
        yield return new WaitForSeconds(0.5f);
        isOkayToParent = true;
    }

    public void UnParentAllChild()
    {
        if (this.transform.childCount > 0)
        {
            foreach (Transform chld in this.transform)
            {
                if (chld.gameObject.tag == "Player")
                {
                    chld.gameObject.transform.SetParent(null, true);
                    break;
                }
            }
        }
    }

}
