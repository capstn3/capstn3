using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PresPlatActivated : MonoBehaviour
{
    public abstract void Activated();
    public abstract void Deactivate();
}
