using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightingFlicker : MonoBehaviour
{

    private bool isFlickering = false;
    public Material blankMaterial;
    private Material originalMaterial;
    public Vector2 randomFlickerDelay;
    private float flickerCooldown;
    private void Start()
    {
        originalMaterial = this.GetComponent<MeshRenderer>().material;
        flickerCooldown = Random.Range(randomFlickerDelay.x, randomFlickerDelay.y);
    }

    void Update()
    {
        if(flickerCooldown<=0)
        {
            StartCoroutine("FlickerLight");
        }
        else
        {
            flickerCooldown -= Time.deltaTime;
        }
    }
    
    IEnumerator FlickerLight()
    {
        this.GetComponent<MeshRenderer>().material = blankMaterial;
        float delay = Random.Range(0.01f, 0.2f);
        yield return new WaitForSeconds(delay);
        this.GetComponent<MeshRenderer>().material = originalMaterial;
        float delay2 = Random.Range(0.01f, 0.2f);
        yield return new WaitForSeconds(delay2);
        this.GetComponent<MeshRenderer>().material = blankMaterial;
        float delay3 = Random.Range(0.01f, 0.2f);
        yield return new WaitForSeconds(delay3);
        this.GetComponent<MeshRenderer>().material = originalMaterial;
        float delay4 = Random.Range(0.01f, 0.2f);
        yield return new WaitForSeconds(delay4);
        flickerCooldown = Random.Range(randomFlickerDelay.x, randomFlickerDelay.y);
    }

}
