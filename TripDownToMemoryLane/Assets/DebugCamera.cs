using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugCamera : MonoBehaviour
{

    InteractionHandler interactionHandler;
    // Start is called before the first frame update
    void Start()
    {
        interactionHandler = SingletonManager.Get<InteractionHandler>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawLine(Camera.main.ScreenToWorldPoint(interactionHandler.GetPlayerControls().Mouse.Position.ReadValue<Vector2>()), Camera.main.transform.forward + new Vector3(0, 0, 1000f));
    }
}
