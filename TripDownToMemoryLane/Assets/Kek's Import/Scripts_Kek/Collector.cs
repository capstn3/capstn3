using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class Collector : MonoBehaviour
{
    [SerializeField] List<Collectible> _collectibles;
    [SerializeField] UnityEvent _onCollectionsComplete;
    


    int _countCollected;

    // Start is called before the first frame update
    void Start()
    {
        foreach(var collectible in _collectibles)
        {
            collectible.OnPickedUpProp += ItemPickedUp;
        }

        int countRemaing = _collectibles.Count - _countCollected;
    }

    public void ItemPickedUp()
    {
        _countCollected++;
        int countRemaining = _collectibles.Count - _countCollected;

        if (countRemaining > 0)
            return;

        _onCollectionsComplete?.Invoke();

    }

   


    void OnValidate()
    {
        _collectibles = _collectibles.Distinct().ToList();
    }
}
