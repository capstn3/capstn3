using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioEffect : MonoBehaviour
{
    [SerializeField] float radiusEffect;
    [SerializeField] LayerMask playerMask;

    private bool stopWhiteNoise;


    private void Awake()
    {
        playerMask = LayerMask.GetMask("Player");

    }

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<AudioSource>().volume = 0;
        GetComponent<AudioSource>().spatialBlend = 1f;
        GetComponent<AudioSource>().maxDistance = 100f;
        GetComponent<AudioSource>().spatialBlend = 1f;
        GetComponent<AudioSource>().rolloffMode = AudioRolloffMode.Linear;
        GetComponent<AudioSource>().dopplerLevel = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if(GetComponent<AudioSource>().volume == 0)
        {
            GetComponent<AudioSource>().enabled = false;
        }
        
        
        Collider[] hitCollider = Physics.OverlapSphere(transform.position, radiusEffect, playerMask);

        foreach(var hit in hitCollider)
        {
            if(hit == null) {
                GetComponent<AudioSource>().enabled = false;
                return; }

            if (hit.GetComponent<PlayerMovements>())
            {
                var player = hit.GetComponent<PlayerMovements>();
                //Debug.Log("Hit Player");
                //Debug.Log(DistanceToOrigin(player));
                if(GetComponent<AudioSource>().enabled == false)
                {
                    GetComponent<AudioSource>().enabled = true;
                }

                if (!hit.GetComponent<PlayerInteract>().isInteractingSliding)
                {
                    GetComponent<AudioSource>().volume = 1 - DistanceToOrigin(player) / radiusEffect;
                }
                else
                {
                    GetComponent<AudioSource>().volume = 0.03f;
                }
                
            }

            
            
           
        }


       

        
        
    }

   

    private float DistanceToOrigin(PlayerMovements player)
    {
        var distance = Vector3.Distance(transform.position, player.transform.position);

        return distance;
    }

    private void OnDrawGizmos()
    {
        //Gizmos.DrawWireSphere(transform.position, radiusEffect);
    }
}
