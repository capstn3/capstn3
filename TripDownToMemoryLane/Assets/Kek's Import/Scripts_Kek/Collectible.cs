﻿using System;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    public event Action OnPickedUpProp;

    public void ItemPickUp()
    {
        OnPickedUpProp?.Invoke();
    }


}