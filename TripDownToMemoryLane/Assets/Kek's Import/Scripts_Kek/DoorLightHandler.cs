using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OnAllLightActive : UnityEvent
{

}


public class DoorLightHandler : MonoBehaviour
{
    public OnAllLightActive onAllLightActive;

    public bool needsLight;
    public int lightsActive;
    public int lightsNeeded;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void LightChecker()
    {
        if (lightsActive <= lightsNeeded)
        {
            lightsActive++;
        }
    }


    public void DoorActive()
    {
        if (needsLight)
        {
            LightChecker();
            if (lightsActive == lightsNeeded)
            {
                onAllLightActive?.Invoke();

            }
        }
    }
}
