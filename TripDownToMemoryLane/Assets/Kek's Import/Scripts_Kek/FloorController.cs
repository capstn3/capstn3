using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorController : MonoBehaviour
{
    public int id;
    public List<GameObject> lightFeedback;

    bool isActive;
    Vector3 originPosition;
    IntensitySphere intensitySphere;

    // Start is called before the first frame update
    void Start()
    {
        originPosition = transform.localPosition;
        GameEvent.current.onProjectorObjectTriggerEnter += OnDoorwayOpen;
        GameEvent.current.onProjectorObjectTriggerExit += OnDoorwayClose;
        //GameEvent.current.onPuzzleSlidingComplete += OnDoorwayOpen;
        //GameEvent.current.onPuzzleSlidingComplete += LightFeedback;
        //intensitySphere.OnLightIntensityHit += IntensityDoorOpen;
        //intensitySphere.OnLightIntensityExit += IntensityDoorClose;
    }

    private void OnDoorwayOpen(int id)
    {
        if (id == this.id)
        {
            LeanTween.moveLocalX(gameObject, originPosition.x, 1).setEaseOutQuad();
        }

    }

    private void OnDoorwayClose(int id)
    {

        if (id == this.id)
        {
            LeanTween.moveLocalX(gameObject, originPosition.x + 8f, 1f).setEaseInQuad();
        }

    }
}
