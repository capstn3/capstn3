using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomLight : MonoBehaviour
{
    public Light _spotLight;

    private float color = 0;
    private float lightFlicker = 0;

    private void Start()
    {
        //Debug.Log("Debug");
        //OpenLight();
        //FlickeringLight();
    }


    public void OpenLight()
    {
        Debug.Log("Open Lights");
        StartCoroutine(FadeInLight());
        StartCoroutine(FlickerLight());
    }

    public void FlickeringLight()
    {
        StartCoroutine(FlickerLight());
    }


    private IEnumerator FadeInLight()
    {
        while(color <= 1f)
        {
            //Debug.Log("Open Lights");

            _spotLight.color = new Color(color, color, color);

            yield return null;

            color += 0.06f;

        }
    }


    private IEnumerator FlickerLight()
    {
        while(lightFlicker <= 1f)
        {
            var rand = Random.Range(0.1f, 0.5f);
            yield return new WaitForSecondsRealtime(0.25f);

            _spotLight.enabled = false;

            yield return new WaitForSecondsRealtime(rand);

            _spotLight.enabled = true;

            yield return new WaitForSecondsRealtime(rand);

            lightFlicker += 0.5f;
        }
     
    }
}
