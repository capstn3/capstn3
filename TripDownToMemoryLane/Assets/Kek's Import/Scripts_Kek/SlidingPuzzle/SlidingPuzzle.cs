﻿using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using DG.Tweening;
using UnityEngine.Events;

public class SlidingPuzzle : MonoBehaviour
{
    public UnityEvent OnCompletePuzzle;
    public UnityEvent OnRaisePuzzle;
    public int id;
    public Texture2D image;
    public GameObject fullImage;
    public int blockPerLine = 4;
    public int blockSize = 3;
    public int shuffleLength = 20;
    public float defaultMoveDuration = .2f;
    public float shuffleMoveDuration = .1f;
    public CinemachineVirtualCamera transitionCamera;

    //Move to its own script
    [SerializeField] bool needsProp;
    [SerializeField] private int propsNeeded;
    [SerializeField] private int propsCollected;

    [SerializeField] bool needsLight;
    [SerializeField] private int lightsNeeded;
    [SerializeField] private int lightsActive;


    enum PuzzleState { Solved, Shuffling, InPlay };
    PuzzleState state;
    bool isSolved;

    private Block emptyBlock;
    Block[,] blocks;

    Queue<Block> inputs;
    private bool blockIsMoving;
    int shuffleMovesRemaining;
    Vector3Int previousShuffleOffset;

    public void ShowFullSlidingPuzzle()
    {
        fullImage.SetActive(true);
        fullImage.GetComponent<MeshRenderer>().material.shader = Shader.Find("Unlit/Texture");
        fullImage.GetComponent<MeshRenderer>().material.mainTexture = image;
        OnCompletePuzzle.Invoke();
        SoundManager.PlaySound(SoundManager.Sound.PuzzleComplete, 0.4f);
        if (GetComponent<TransitionPuzzle>())
        {
            GameEvent.current.PuzzleSlidingComplete();

        }

        if (GetComponent<TransitionPuzzleNoMemory>())
        {
            GameEvent.current.PuzzleSlidingCompleteNoMemory(id);
        }
    }

    private void Start()
    {
        fullImage.GetComponent<MeshRenderer>().material.shader = Shader.Find("Unlit/Texture");
        fullImage.GetComponent<MeshRenderer>().material.mainTexture = image;

        CreatePuzzle();

        StartShuffle();

        GameEvent.current.onCollectedProps += RaisePuzzlePlatform;
    }

    private void Update()
    {

    }

    public void RaiseSlidingPuzzleSound()
    {
        SoundManager.PlaySound(SoundManager.Sound.SlidingPuzzleElevate, 0.1f);
    }

    private void PropsChecker()
    {
        if(propsCollected <= propsNeeded)
        {
            propsCollected++;
        }
    }

    private void LightChecker()
    {
        if(lightsActive <= lightsNeeded)
        {
            lightsActive++;
        }
    }

    //Move to its own script
    private void RaisePuzzlePlatform()
    {
        if (needsProp)
        {
            
            PropsChecker();
            if (propsCollected == propsNeeded)
            {
                RaiseSlidingPuzzleSound();
                if (CameraTransition.Instance != null)
                {
                    CameraTransition.Instance.Transition(transitionCamera);
                }
                
                this.gameObject.transform.DOMoveY(transform.position.y + 2f, 3f);
            }
        }
       

    }

    public void RaisePuzzlePlatformWithRayLight()
    {
        if (needsLight)
        {
            
            LightChecker();
            if(lightsActive == lightsNeeded)
            {
                RaiseSlidingPuzzleSound();
                OnRaisePuzzle.Invoke();
                if (CameraTransition.Instance != null)
                {
                    CameraTransition.Instance.Transition(transitionCamera);
                }

                this.gameObject.transform.DOMoveY(transform.position.y + 2f, 3f);
            }
        }
    }

    public void RaisePuzzlePlatformViaLightPressure()
    {
        Debug.Log("Test");
        RaiseSlidingPuzzleSound();
        this.gameObject.transform.DOMoveY(transform.position.y + 2f, 3f);
    }



    private void CreatePuzzle()
    {
        //Debug.Log("create");
        blocks = new Block[blockPerLine, blockPerLine];
        Texture2D[,] imageSlices = ImageSlicer.GetSlices(image, blockPerLine);
        for (int z = 0; z < blockPerLine; z++)
        {
            for (int x = 0; x < blockPerLine; x++)
            {
                GameObject blockObject = GameObject.CreatePrimitive(PrimitiveType.Quad);
                //blockObject.layer = 17;
                blockObject.tag = "Puzzle";
                Vector3 size = new Vector3(blockSize, blockSize, blockSize);
                blockObject.transform.localScale = size;
                Vector3 blockPosition = -new Vector3(1, 0, 1) * (blockPerLine - 1) * .5f + new Vector3(x, 0, z);
                blockObject.transform.position = blockPosition * blockSize + transform.position ;
                blockObject.transform.rotation = Quaternion.Euler(90, 0, 0);
                blockObject.transform.parent = transform;

                Block block = blockObject.AddComponent<Block>();
                block.OnBlockPressed += PlayerMoveBlockInput;
                block.OnFinishMoving += OnBlockFinishMoving;
                block.Init(new Vector3Int(x,0, z), imageSlices[x, z]);
                blocks[x, z] = block;

                if(z == 0 && x == blockPerLine -1)
                {
                    blockObject.SetActive(false);
                    emptyBlock = block;
                }
            }
        }

        inputs = new Queue<Block>();

    }

    private void PlayerMoveBlockInput(Block blockToMove)
    {
        if(state == PuzzleState.InPlay)
        {
            inputs.Enqueue(blockToMove);
            MakeNextPlayerMove();
        }
        

    }

    private void MoveBlock(Block blockToMove, float duration)
    {
        if ((blockToMove.coord - emptyBlock.coord).sqrMagnitude == 1)
        {
            if (state == PuzzleState.InPlay)
                SoundManager.PlaySound(SoundManager.Sound.SlidingPuzzle, 0.5f);

            blocks[blockToMove.coord.x, blockToMove.coord.z] = emptyBlock;
            blocks[emptyBlock.coord.x, emptyBlock.coord.z] = blockToMove;

            Vector3Int targetCoord = emptyBlock.coord;
            emptyBlock.coord = blockToMove.coord;
            blockToMove.coord = targetCoord;
            Vector3 targetPosition = emptyBlock.transform.position;
            emptyBlock.transform.position = blockToMove.transform.position;
            //blockToMove.transform.position = targetPosition;
            blockToMove.MoveToPosition(targetPosition, duration);
            blockIsMoving = true;
        }
    }

    private void OnBlockFinishMoving()
    {
        blockIsMoving = false;
        CheckIfSolved();
        if (state == PuzzleState.InPlay)
        {
            MakeNextPlayerMove();
        }
        else if(state == PuzzleState.Shuffling)
        {
            if (shuffleMovesRemaining > 0)
            {
                MakeNextShuffleMove();
            }
            else
            {
                state = PuzzleState.InPlay;
            }
        }
        

        
    }

    private void MakeNextPlayerMove()
    {
        while (inputs.Count > 0 && !blockIsMoving)
        {
            MoveBlock(inputs.Dequeue(), defaultMoveDuration);
        }
    }

    private void StartShuffle()
    {
        state = PuzzleState.Shuffling;
        shuffleMovesRemaining = shuffleLength;
        MakeNextShuffleMove();
    }

    private void MakeNextShuffleMove()
    {
        Vector3Int[] offsets = { new Vector3Int(1, 0, 0), new Vector3Int(-1, 0, 0), new Vector3Int(0, 0, 1), new Vector3Int(0, 0, -1) };
        int randomIndex = Random.Range(0, offsets.Length);

        for(int i = 0; i < offsets.Length; i++)
        {
            Vector3Int offset = offsets[(randomIndex + i) % offsets.Length];
            if(offset != previousShuffleOffset * -1)
            {
                Vector3Int moveBlockCoord = emptyBlock.coord + offset;

                if (moveBlockCoord.x >= 0 && moveBlockCoord.x < blockPerLine && moveBlockCoord.z >= 0 && moveBlockCoord.z < blockPerLine)
                {
                    MoveBlock(blocks[moveBlockCoord.x, moveBlockCoord.z], shuffleMoveDuration);
                    shuffleMovesRemaining--;
                    previousShuffleOffset = offset;
                    break;
                }
            }
           
        }

    }

    private void CheckIfSolved()
    {
        foreach(Block block in blocks)
        {
            if (!block.IsAtStartingCoord())
            {
                return;
            }
        }

        state = PuzzleState.Solved;
        isSolved = true;
        OnCompletePuzzle.Invoke();
        emptyBlock.gameObject.SetActive(true);
        GameEvent.current.PuzzleSlidingComplete(id);
        SoundManager.PlaySound(SoundManager.Sound.PuzzleComplete, 0.7f);

        if (GetComponent<TransitionPuzzle>())
        {
            GameEvent.current.PuzzleSlidingComplete();
            
        }

        if (GetComponent<TransitionPuzzleNoMemory>())
        {
            GameEvent.current.PuzzleSlidingCompleteNoMemory(id);
        }

    }

    public bool GetIsSolved()
    {
        return isSolved;
    }

    
}
