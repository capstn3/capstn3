﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Block : MonoBehaviour
{
    public event System.Action<Block> OnBlockPressed;
    public event System.Action OnFinishMoving;

    public Vector3Int coord;
    Vector3Int startingCoord;


    public void Init(Vector3Int startingCoord, Texture2D image)
    {
        this.startingCoord = startingCoord;
        coord = startingCoord;

        GetComponent<MeshRenderer>().material.shader = Shader.Find("Unlit/Texture");
        GetComponent<MeshRenderer>().material.mainTexture = image;
    }

    public void MoveToPosition(Vector3 targetPosition, float duration)
    {

        StartCoroutine(AnimateMove(targetPosition, duration));
    }


    public void OnMouseDown()
    {

        Debug.Log("MousePress");
        OnBlockPressed?.Invoke(this);
    }

    


    IEnumerator AnimateMove(Vector3 targetPosition, float duration)
    {
        Vector3 initialPos = transform.position;
        float percent = 0;
        while (percent < 1)
        {

            percent += Time.deltaTime / duration;
            transform.position = Vector3.Lerp(initialPos, targetPosition, percent);

            yield return null;
        }

        OnFinishMoving?.Invoke();
    }


    public bool IsAtStartingCoord()
    {
        return coord == startingCoord;
    }
}
