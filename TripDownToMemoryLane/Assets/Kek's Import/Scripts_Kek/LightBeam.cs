﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightBeam : MonoBehaviour
{
    [SerializeField] private Transform lightBeam;

    [SerializeField] private bool isActive = false;
    private LineRenderer beam;
    private Transform particleFX;

    private void Awake()
    {
        beam = lightBeam.transform.Find("Laser").GetComponent<LineRenderer>();
        particleFX = lightBeam.transform.Find("Beam");
    }

    private void Start()
    {
        if (!isActive)
        {
            DisableBeam();
        }
    }

    public void EnableBeam()
    {
        beam.enabled = true;
        particleFX.gameObject.SetActive(true);
    }

    public void DisableBeam()
    {
        beam.enabled = false;
        particleFX.gameObject.SetActive(false);
    }
}
