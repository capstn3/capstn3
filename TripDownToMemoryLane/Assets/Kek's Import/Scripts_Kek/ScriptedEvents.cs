using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OnTrggerEnterScriptedEvent : UnityEvent
{

}


public class ScriptedEvents : MonoBehaviour
{
    public OnTrggerEnterScriptedEvent onScriptedEvent;

    private void OnTriggerEnter(Collider other)
    {
       if(other.gameObject.tag == "Player")
        {
            onScriptedEvent?.Invoke();
        }
    }
}
