﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class PuzzleDetected : UnityEvent
{
    
}

public class InteractTrigger : MonoBehaviour
{
    [SerializeField] private GameObject puzzle;
    public PuzzleDetected puzzleDetected;
    public PuzzleDetected puzzleExitTrigger;


    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerInteract>())
        {
            other.GetComponent<PlayerInteract>().CanInteract = true;
            InteractableManager.instance.AddSpecialIndicator(other.transform, this.transform);
            puzzleDetected.Invoke();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<PlayerInteract>())
        {
            other.GetComponent<PlayerInteract>().CanInteract = false;
            InteractableManager.instance.ClearSpecialIndicators();
            puzzleExitTrigger.Invoke();
        }
    }
}
