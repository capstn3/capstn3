using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlitchSpikes : MonoBehaviour
{
    public float glitchDuration = 2f;
    public float glitchInterval = 5f;

    [SerializeField] AudioClip[] glitchSoundClip;

    private Transform normal;
    private Transform glitch;

    AudioSource _audioSource;

    private void Awake()
    {
        normal = transform.Find("NormalSpikes");
        glitch = transform.Find("GlitchSpikes");

        CheckAudioSource();
    }

    private void CheckAudioSource()
    {
        if (GetComponent<AudioSource>() == null)
        {
            gameObject.AddComponent<AudioSource>();
        }
        _audioSource = GetComponent<AudioSource>();
    }


    // Start is called before the first frame update
    void Start()
    {
        normal.gameObject.SetActive(true);
        glitch.gameObject.SetActive(false);

        StartCoroutine(GlitchMechanic());
    }

    // Update is called once per frame
    void Update()
    {

    }

    private IEnumerator GlitchMechanic()
    {
        glitch.gameObject.SetActive(false);
        normal.gameObject.SetActive(true);
        _audioSource.loop = false;
        _audioSource.Stop();


        yield return new WaitForSeconds(glitchInterval);

        StartCoroutine(GlitchDuration());
        StopCoroutine(GlitchMechanic());

    }


    private IEnumerator GlitchDuration()
    {
        normal.gameObject.SetActive(false);
        glitch.gameObject.SetActive(true);
        _audioSource.loop = true;
        var index = UnityEngine.Random.Range(0, glitchSoundClip.Length - 1);
        _audioSource.PlayOneShot(glitchSoundClip[index]);

        yield return new WaitForSeconds(glitchDuration);

        StartCoroutine(GlitchMechanic());
        StopCoroutine(GlitchDuration());


    }
}
