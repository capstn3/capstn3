﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectorObject : MonoBehaviour
{
    public bool doesNotNeedLNode = true;
    public int id;
    public LayerMask layer;

    bool isLinedUp = false;
    bool isActiveEnter = false;
    bool isActiveExit = false;

    private bool onCorrectIntensity = false;

    // Start is called before the first frame update
    void Start()
    {
        EquipmentChangesEvents.instance.changeLightNodeIntensity += CheckLightNodeIntensity;
    }

    // Update is called once per frame
    void Update()
    {
        if(doesNotNeedLNode)
        {
            DetectShadowLinedUp();
        }
        else if (onCorrectIntensity)
        {
            DetectShadowLinedUp();
        }

    }

    private void DetectShadowLinedUp()
    {
        RaycastHit hit;

        if (Physics.Raycast(gameObject.transform.position, gameObject.transform.forward, out hit, 100f, layer))
        {
            isLinedUp = true;
            if (isLinedUp && !isActiveEnter)
            {
                isActiveExit = false;
                isActiveEnter = true;
                GameEvent.current.ProjectorObjectTriggerEnter(id);
            }

        }
        else
        {
            isLinedUp = false;
            if(!isLinedUp && !isActiveExit)
            {
                isActiveEnter = false;
                isActiveExit = true;
                GameEvent.current.ProjectObjecttriggerExit(id);
            }
        }

    }

    private void CheckLightNodeIntensity(int ckId, bool ckInten)
    {
        if(ckId == id)
        {
            onCorrectIntensity = ckInten;
        }
    }

}
