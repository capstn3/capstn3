using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlitchBehaviour : MonoBehaviour
{
    public float maxDistanceX = 5f;
    public float maxDistanceY = 5f;
    public float maxDistanceZ = 5f;
    
    public float glitchEffectInterval = 0.5f;

    private float xPosition;
    private float zPosition;



    // Start is called before the first frame update
    void Start()
    {
       
        
    }

    // Update is called once per frame
    void Update()
    {
        glitchEffectInterval -= Time.deltaTime;
        if(glitchEffectInterval <= 0)
        {
            transform.localPosition = new Vector3(Random.Range(-maxDistanceX,maxDistanceX), Random.Range(-maxDistanceY, maxDistanceY), Random.Range(-maxDistanceZ,  maxDistanceZ));
            glitchEffectInterval = 5f;
        }

    }


   
}
