// GENERATED AUTOMATICALLY FROM 'Assets/Kek's Import/PlayerControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerControls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerControls"",
    ""maps"": [
        {
            ""name"": ""PlayerControl"",
            ""id"": ""7e508163-c401-4a6c-88b3-91966b9678f2"",
            ""actions"": [
                {
                    ""name"": ""FowardMovement"",
                    ""type"": ""PassThrough"",
                    ""id"": ""26f617f2-3792-409f-9930-23806c16a09e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SidewayMovement"",
                    ""type"": ""PassThrough"",
                    ""id"": ""f1f646ef-56d6-41c4-a204-7bd456b0705e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""606fd0d5-52da-4297-8e82-4beee6d17bd9"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""Forward"",
                    ""id"": ""5d5862d5-66c3-43fb-a580-430dc4b7f71f"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""FowardMovement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""9021c740-f1c3-40b6-b53c-c634f3b3d9d7"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""FowardMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""618beb2a-7d09-40de-a5d4-0f6b5176b054"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""FowardMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""5d3de4ff-1f11-4c9a-980c-0d7d7459b93d"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Sideway"",
                    ""id"": ""c6db3cf0-d6d6-4681-b514-cf5ab775ab1b"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SidewayMovement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""c1daa327-e28a-47f3-bc8e-f317c40e76db"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SidewayMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""7689079f-2f74-4265-a980-df9040b95c6f"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SidewayMovement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                }
            ]
        },
        {
            ""name"": ""PlayerInteraction"",
            ""id"": ""74c67188-2ab5-4e32-b2c7-c84ac6f24fb6"",
            ""actions"": [
                {
                    ""name"": ""Interact"",
                    ""type"": ""PassThrough"",
                    ""id"": ""861a6f7b-ee23-481f-8a96-86922d5c7b1a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""ExitPuzzle"",
                    ""type"": ""PassThrough"",
                    ""id"": ""d86c17cb-449e-48a4-9159-5348a2ea8a89"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""TurnRayLight"",
                    ""type"": ""Button"",
                    ""id"": ""203c3290-6456-48ee-84c2-50b58b4b8c71"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""DialogueProceed"",
                    ""type"": ""Button"",
                    ""id"": ""7bdf81ec-60b3-4e03-b607-b6f4f001eee1"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""InteractItem"",
                    ""type"": ""Button"",
                    ""id"": ""54ec6dff-4c45-41a6-b624-6339307c1d76"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""TestingButton"",
                    ""type"": ""Button"",
                    ""id"": ""5751b580-f079-4604-86c5-f17db747db58"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""IncreaseIntensity"",
                    ""type"": ""Button"",
                    ""id"": ""72becb83-9a7b-417e-a2a1-64d2be9aa1dc"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""DecreaseIntensity"",
                    ""type"": ""Button"",
                    ""id"": ""a3de0564-7f5a-4863-b281-75e30d44feb3"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""AutoCompletePuzzle"",
                    ""type"": ""Button"",
                    ""id"": ""0c8a3d02-96a7-4669-a612-58b5e59eda14"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""5131e2d1-c9a9-47df-9019-192216c300d4"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""632a1e41-da85-4071-9ad7-e132bc428514"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ExitPuzzle"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Horizontal"",
                    ""id"": ""a3589d63-4a2a-46af-ba63-b8ff31185726"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""TurnRayLight"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""5118a960-f9c3-4dc8-8b41-13ff2f0807fa"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""TurnRayLight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""cb8694a1-0f6e-447f-9748-c1be5775d0dc"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""TurnRayLight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""dc1bd5b7-057b-4260-8992-5f081dc3c382"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DialogueProceed"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2e8d0dc0-f0e8-4bc7-88b0-d17015ba2f76"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""InteractItem"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""af423453-a184-4f93-bec6-93574d742904"",
                    ""path"": ""<Keyboard>/p"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""TestingButton"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""825e4373-3251-4fca-b837-8526772687ea"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""IncreaseIntensity"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a4f9683b-a868-465e-888e-de5bf9df96d9"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DecreaseIntensity"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Button With One Modifier"",
                    ""id"": ""d874de7f-bcf4-4db0-a28e-2c5926dec59f"",
                    ""path"": ""ButtonWithOneModifier"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AutoCompletePuzzle"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""modifier"",
                    ""id"": ""8a83c7a8-9ac4-4f1a-9b4e-3000c7b8726f"",
                    ""path"": ""<Keyboard>/ctrl"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AutoCompletePuzzle"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""button"",
                    ""id"": ""ca5c3c3c-50d9-4f9d-a3bf-fe662f7673c5"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""AutoCompletePuzzle"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                }
            ]
        },
        {
            ""name"": ""Mouse"",
            ""id"": ""37726d8b-ebf8-4009-a6d7-ab090d7b7677"",
            ""actions"": [
                {
                    ""name"": ""Click"",
                    ""type"": ""Button"",
                    ""id"": ""9e9349c9-a49d-4439-83cd-5b4682d2efc6"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Position"",
                    ""type"": ""PassThrough"",
                    ""id"": ""15ce9962-0a5f-44ee-b72d-89b283e5abaf"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""RightClick"",
                    ""type"": ""Button"",
                    ""id"": ""0ea055b9-5b22-4670-a8b5-8e983b64d347"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""DebugPosition"",
                    ""type"": ""Button"",
                    ""id"": ""fd26f506-a0e5-42ac-9cea-6bf241ab284d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""75330391-1a54-40de-b106-b7e51826d24d"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Click"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""df715ac8-c1b5-4515-bfb7-1dcfe5f00969"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Position"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4689355f-c274-4ae2-a9b7-ce92b5ca4923"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RightClick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1edbd2fe-8a55-4d52-ae9e-6eecc56b9b90"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DebugPosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Equipments"",
            ""id"": ""747018de-33c0-43ea-be0a-4277c30668d2"",
            ""actions"": [
                {
                    ""name"": ""ChangeToShutterSpeed"",
                    ""type"": ""Button"",
                    ""id"": ""8b8a11e6-9d7b-421f-b573-683462727fc1"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ChangeToAperture"",
                    ""type"": ""Button"",
                    ""id"": ""9db1be68-8a8a-4ade-87a7-50cc064dac7c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ChangeToWhiteBalance"",
                    ""type"": ""Button"",
                    ""id"": ""6ae57da7-2be8-4a69-9cdd-737462ed6409"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Increase"",
                    ""type"": ""Button"",
                    ""id"": ""895e0fba-c459-48b0-baba-482adb6783be"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Decrease"",
                    ""type"": ""Button"",
                    ""id"": ""272c0061-46e0-4557-85cb-8f0864673d3a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ResetEquipment"",
                    ""type"": ""Button"",
                    ""id"": ""085b17a6-89dd-48aa-b14f-7938bace80b0"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""e1d95683-9a69-4435-bdc3-51a720e47474"",
                    ""path"": ""<Keyboard>/1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ChangeToShutterSpeed"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4960ad0c-2e3d-4a5e-997d-b11d653fefb4"",
                    ""path"": ""<Keyboard>/2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ChangeToAperture"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cf8a4174-8ee9-43e8-8455-21b0c7b7fc30"",
                    ""path"": ""<Keyboard>/3"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ChangeToWhiteBalance"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b804ea17-0664-4a6a-9275-d37198d27f53"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Increase"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6ce38689-6d98-4aa2-8e2a-3856a3d3057a"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Decrease"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Button With One Modifier"",
                    ""id"": ""38f41941-9296-4f7b-883d-364732a6511d"",
                    ""path"": ""ButtonWithOneModifier"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ResetEquipment"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""modifier"",
                    ""id"": ""d00af13a-d59c-4300-81cd-5979023a884c"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ResetEquipment"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""button"",
                    ""id"": ""2c57f641-6d53-46f8-b44e-42f1325eef9c"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ResetEquipment"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // PlayerControl
        m_PlayerControl = asset.FindActionMap("PlayerControl", throwIfNotFound: true);
        m_PlayerControl_FowardMovement = m_PlayerControl.FindAction("FowardMovement", throwIfNotFound: true);
        m_PlayerControl_SidewayMovement = m_PlayerControl.FindAction("SidewayMovement", throwIfNotFound: true);
        m_PlayerControl_Jump = m_PlayerControl.FindAction("Jump", throwIfNotFound: true);
        // PlayerInteraction
        m_PlayerInteraction = asset.FindActionMap("PlayerInteraction", throwIfNotFound: true);
        m_PlayerInteraction_Interact = m_PlayerInteraction.FindAction("Interact", throwIfNotFound: true);
        m_PlayerInteraction_ExitPuzzle = m_PlayerInteraction.FindAction("ExitPuzzle", throwIfNotFound: true);
        m_PlayerInteraction_TurnRayLight = m_PlayerInteraction.FindAction("TurnRayLight", throwIfNotFound: true);
        m_PlayerInteraction_DialogueProceed = m_PlayerInteraction.FindAction("DialogueProceed", throwIfNotFound: true);
        m_PlayerInteraction_InteractItem = m_PlayerInteraction.FindAction("InteractItem", throwIfNotFound: true);
        m_PlayerInteraction_TestingButton = m_PlayerInteraction.FindAction("TestingButton", throwIfNotFound: true);
        m_PlayerInteraction_IncreaseIntensity = m_PlayerInteraction.FindAction("IncreaseIntensity", throwIfNotFound: true);
        m_PlayerInteraction_DecreaseIntensity = m_PlayerInteraction.FindAction("DecreaseIntensity", throwIfNotFound: true);
        m_PlayerInteraction_AutoCompletePuzzle = m_PlayerInteraction.FindAction("AutoCompletePuzzle", throwIfNotFound: true);
        // Mouse
        m_Mouse = asset.FindActionMap("Mouse", throwIfNotFound: true);
        m_Mouse_Click = m_Mouse.FindAction("Click", throwIfNotFound: true);
        m_Mouse_Position = m_Mouse.FindAction("Position", throwIfNotFound: true);
        m_Mouse_RightClick = m_Mouse.FindAction("RightClick", throwIfNotFound: true);
        m_Mouse_DebugPosition = m_Mouse.FindAction("DebugPosition", throwIfNotFound: true);
        // Equipments
        m_Equipments = asset.FindActionMap("Equipments", throwIfNotFound: true);
        m_Equipments_ChangeToShutterSpeed = m_Equipments.FindAction("ChangeToShutterSpeed", throwIfNotFound: true);
        m_Equipments_ChangeToAperture = m_Equipments.FindAction("ChangeToAperture", throwIfNotFound: true);
        m_Equipments_ChangeToWhiteBalance = m_Equipments.FindAction("ChangeToWhiteBalance", throwIfNotFound: true);
        m_Equipments_Increase = m_Equipments.FindAction("Increase", throwIfNotFound: true);
        m_Equipments_Decrease = m_Equipments.FindAction("Decrease", throwIfNotFound: true);
        m_Equipments_ResetEquipment = m_Equipments.FindAction("ResetEquipment", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // PlayerControl
    private readonly InputActionMap m_PlayerControl;
    private IPlayerControlActions m_PlayerControlActionsCallbackInterface;
    private readonly InputAction m_PlayerControl_FowardMovement;
    private readonly InputAction m_PlayerControl_SidewayMovement;
    private readonly InputAction m_PlayerControl_Jump;
    public struct PlayerControlActions
    {
        private @PlayerControls m_Wrapper;
        public PlayerControlActions(@PlayerControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @FowardMovement => m_Wrapper.m_PlayerControl_FowardMovement;
        public InputAction @SidewayMovement => m_Wrapper.m_PlayerControl_SidewayMovement;
        public InputAction @Jump => m_Wrapper.m_PlayerControl_Jump;
        public InputActionMap Get() { return m_Wrapper.m_PlayerControl; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerControlActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerControlActions instance)
        {
            if (m_Wrapper.m_PlayerControlActionsCallbackInterface != null)
            {
                @FowardMovement.started -= m_Wrapper.m_PlayerControlActionsCallbackInterface.OnFowardMovement;
                @FowardMovement.performed -= m_Wrapper.m_PlayerControlActionsCallbackInterface.OnFowardMovement;
                @FowardMovement.canceled -= m_Wrapper.m_PlayerControlActionsCallbackInterface.OnFowardMovement;
                @SidewayMovement.started -= m_Wrapper.m_PlayerControlActionsCallbackInterface.OnSidewayMovement;
                @SidewayMovement.performed -= m_Wrapper.m_PlayerControlActionsCallbackInterface.OnSidewayMovement;
                @SidewayMovement.canceled -= m_Wrapper.m_PlayerControlActionsCallbackInterface.OnSidewayMovement;
                @Jump.started -= m_Wrapper.m_PlayerControlActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_PlayerControlActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_PlayerControlActionsCallbackInterface.OnJump;
            }
            m_Wrapper.m_PlayerControlActionsCallbackInterface = instance;
            if (instance != null)
            {
                @FowardMovement.started += instance.OnFowardMovement;
                @FowardMovement.performed += instance.OnFowardMovement;
                @FowardMovement.canceled += instance.OnFowardMovement;
                @SidewayMovement.started += instance.OnSidewayMovement;
                @SidewayMovement.performed += instance.OnSidewayMovement;
                @SidewayMovement.canceled += instance.OnSidewayMovement;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
            }
        }
    }
    public PlayerControlActions @PlayerControl => new PlayerControlActions(this);

    // PlayerInteraction
    private readonly InputActionMap m_PlayerInteraction;
    private IPlayerInteractionActions m_PlayerInteractionActionsCallbackInterface;
    private readonly InputAction m_PlayerInteraction_Interact;
    private readonly InputAction m_PlayerInteraction_ExitPuzzle;
    private readonly InputAction m_PlayerInteraction_TurnRayLight;
    private readonly InputAction m_PlayerInteraction_DialogueProceed;
    private readonly InputAction m_PlayerInteraction_InteractItem;
    private readonly InputAction m_PlayerInteraction_TestingButton;
    private readonly InputAction m_PlayerInteraction_IncreaseIntensity;
    private readonly InputAction m_PlayerInteraction_DecreaseIntensity;
    private readonly InputAction m_PlayerInteraction_AutoCompletePuzzle;
    public struct PlayerInteractionActions
    {
        private @PlayerControls m_Wrapper;
        public PlayerInteractionActions(@PlayerControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Interact => m_Wrapper.m_PlayerInteraction_Interact;
        public InputAction @ExitPuzzle => m_Wrapper.m_PlayerInteraction_ExitPuzzle;
        public InputAction @TurnRayLight => m_Wrapper.m_PlayerInteraction_TurnRayLight;
        public InputAction @DialogueProceed => m_Wrapper.m_PlayerInteraction_DialogueProceed;
        public InputAction @InteractItem => m_Wrapper.m_PlayerInteraction_InteractItem;
        public InputAction @TestingButton => m_Wrapper.m_PlayerInteraction_TestingButton;
        public InputAction @IncreaseIntensity => m_Wrapper.m_PlayerInteraction_IncreaseIntensity;
        public InputAction @DecreaseIntensity => m_Wrapper.m_PlayerInteraction_DecreaseIntensity;
        public InputAction @AutoCompletePuzzle => m_Wrapper.m_PlayerInteraction_AutoCompletePuzzle;
        public InputActionMap Get() { return m_Wrapper.m_PlayerInteraction; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerInteractionActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerInteractionActions instance)
        {
            if (m_Wrapper.m_PlayerInteractionActionsCallbackInterface != null)
            {
                @Interact.started -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnInteract;
                @Interact.performed -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnInteract;
                @Interact.canceled -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnInteract;
                @ExitPuzzle.started -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnExitPuzzle;
                @ExitPuzzle.performed -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnExitPuzzle;
                @ExitPuzzle.canceled -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnExitPuzzle;
                @TurnRayLight.started -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnTurnRayLight;
                @TurnRayLight.performed -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnTurnRayLight;
                @TurnRayLight.canceled -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnTurnRayLight;
                @DialogueProceed.started -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnDialogueProceed;
                @DialogueProceed.performed -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnDialogueProceed;
                @DialogueProceed.canceled -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnDialogueProceed;
                @InteractItem.started -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnInteractItem;
                @InteractItem.performed -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnInteractItem;
                @InteractItem.canceled -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnInteractItem;
                @TestingButton.started -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnTestingButton;
                @TestingButton.performed -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnTestingButton;
                @TestingButton.canceled -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnTestingButton;
                @IncreaseIntensity.started -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnIncreaseIntensity;
                @IncreaseIntensity.performed -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnIncreaseIntensity;
                @IncreaseIntensity.canceled -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnIncreaseIntensity;
                @DecreaseIntensity.started -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnDecreaseIntensity;
                @DecreaseIntensity.performed -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnDecreaseIntensity;
                @DecreaseIntensity.canceled -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnDecreaseIntensity;
                @AutoCompletePuzzle.started -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnAutoCompletePuzzle;
                @AutoCompletePuzzle.performed -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnAutoCompletePuzzle;
                @AutoCompletePuzzle.canceled -= m_Wrapper.m_PlayerInteractionActionsCallbackInterface.OnAutoCompletePuzzle;
            }
            m_Wrapper.m_PlayerInteractionActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Interact.started += instance.OnInteract;
                @Interact.performed += instance.OnInteract;
                @Interact.canceled += instance.OnInteract;
                @ExitPuzzle.started += instance.OnExitPuzzle;
                @ExitPuzzle.performed += instance.OnExitPuzzle;
                @ExitPuzzle.canceled += instance.OnExitPuzzle;
                @TurnRayLight.started += instance.OnTurnRayLight;
                @TurnRayLight.performed += instance.OnTurnRayLight;
                @TurnRayLight.canceled += instance.OnTurnRayLight;
                @DialogueProceed.started += instance.OnDialogueProceed;
                @DialogueProceed.performed += instance.OnDialogueProceed;
                @DialogueProceed.canceled += instance.OnDialogueProceed;
                @InteractItem.started += instance.OnInteractItem;
                @InteractItem.performed += instance.OnInteractItem;
                @InteractItem.canceled += instance.OnInteractItem;
                @TestingButton.started += instance.OnTestingButton;
                @TestingButton.performed += instance.OnTestingButton;
                @TestingButton.canceled += instance.OnTestingButton;
                @IncreaseIntensity.started += instance.OnIncreaseIntensity;
                @IncreaseIntensity.performed += instance.OnIncreaseIntensity;
                @IncreaseIntensity.canceled += instance.OnIncreaseIntensity;
                @DecreaseIntensity.started += instance.OnDecreaseIntensity;
                @DecreaseIntensity.performed += instance.OnDecreaseIntensity;
                @DecreaseIntensity.canceled += instance.OnDecreaseIntensity;
                @AutoCompletePuzzle.started += instance.OnAutoCompletePuzzle;
                @AutoCompletePuzzle.performed += instance.OnAutoCompletePuzzle;
                @AutoCompletePuzzle.canceled += instance.OnAutoCompletePuzzle;
            }
        }
    }
    public PlayerInteractionActions @PlayerInteraction => new PlayerInteractionActions(this);

    // Mouse
    private readonly InputActionMap m_Mouse;
    private IMouseActions m_MouseActionsCallbackInterface;
    private readonly InputAction m_Mouse_Click;
    private readonly InputAction m_Mouse_Position;
    private readonly InputAction m_Mouse_RightClick;
    private readonly InputAction m_Mouse_DebugPosition;
    public struct MouseActions
    {
        private @PlayerControls m_Wrapper;
        public MouseActions(@PlayerControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Click => m_Wrapper.m_Mouse_Click;
        public InputAction @Position => m_Wrapper.m_Mouse_Position;
        public InputAction @RightClick => m_Wrapper.m_Mouse_RightClick;
        public InputAction @DebugPosition => m_Wrapper.m_Mouse_DebugPosition;
        public InputActionMap Get() { return m_Wrapper.m_Mouse; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MouseActions set) { return set.Get(); }
        public void SetCallbacks(IMouseActions instance)
        {
            if (m_Wrapper.m_MouseActionsCallbackInterface != null)
            {
                @Click.started -= m_Wrapper.m_MouseActionsCallbackInterface.OnClick;
                @Click.performed -= m_Wrapper.m_MouseActionsCallbackInterface.OnClick;
                @Click.canceled -= m_Wrapper.m_MouseActionsCallbackInterface.OnClick;
                @Position.started -= m_Wrapper.m_MouseActionsCallbackInterface.OnPosition;
                @Position.performed -= m_Wrapper.m_MouseActionsCallbackInterface.OnPosition;
                @Position.canceled -= m_Wrapper.m_MouseActionsCallbackInterface.OnPosition;
                @RightClick.started -= m_Wrapper.m_MouseActionsCallbackInterface.OnRightClick;
                @RightClick.performed -= m_Wrapper.m_MouseActionsCallbackInterface.OnRightClick;
                @RightClick.canceled -= m_Wrapper.m_MouseActionsCallbackInterface.OnRightClick;
                @DebugPosition.started -= m_Wrapper.m_MouseActionsCallbackInterface.OnDebugPosition;
                @DebugPosition.performed -= m_Wrapper.m_MouseActionsCallbackInterface.OnDebugPosition;
                @DebugPosition.canceled -= m_Wrapper.m_MouseActionsCallbackInterface.OnDebugPosition;
            }
            m_Wrapper.m_MouseActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Click.started += instance.OnClick;
                @Click.performed += instance.OnClick;
                @Click.canceled += instance.OnClick;
                @Position.started += instance.OnPosition;
                @Position.performed += instance.OnPosition;
                @Position.canceled += instance.OnPosition;
                @RightClick.started += instance.OnRightClick;
                @RightClick.performed += instance.OnRightClick;
                @RightClick.canceled += instance.OnRightClick;
                @DebugPosition.started += instance.OnDebugPosition;
                @DebugPosition.performed += instance.OnDebugPosition;
                @DebugPosition.canceled += instance.OnDebugPosition;
            }
        }
    }
    public MouseActions @Mouse => new MouseActions(this);

    // Equipments
    private readonly InputActionMap m_Equipments;
    private IEquipmentsActions m_EquipmentsActionsCallbackInterface;
    private readonly InputAction m_Equipments_ChangeToShutterSpeed;
    private readonly InputAction m_Equipments_ChangeToAperture;
    private readonly InputAction m_Equipments_ChangeToWhiteBalance;
    private readonly InputAction m_Equipments_Increase;
    private readonly InputAction m_Equipments_Decrease;
    private readonly InputAction m_Equipments_ResetEquipment;
    public struct EquipmentsActions
    {
        private @PlayerControls m_Wrapper;
        public EquipmentsActions(@PlayerControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @ChangeToShutterSpeed => m_Wrapper.m_Equipments_ChangeToShutterSpeed;
        public InputAction @ChangeToAperture => m_Wrapper.m_Equipments_ChangeToAperture;
        public InputAction @ChangeToWhiteBalance => m_Wrapper.m_Equipments_ChangeToWhiteBalance;
        public InputAction @Increase => m_Wrapper.m_Equipments_Increase;
        public InputAction @Decrease => m_Wrapper.m_Equipments_Decrease;
        public InputAction @ResetEquipment => m_Wrapper.m_Equipments_ResetEquipment;
        public InputActionMap Get() { return m_Wrapper.m_Equipments; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(EquipmentsActions set) { return set.Get(); }
        public void SetCallbacks(IEquipmentsActions instance)
        {
            if (m_Wrapper.m_EquipmentsActionsCallbackInterface != null)
            {
                @ChangeToShutterSpeed.started -= m_Wrapper.m_EquipmentsActionsCallbackInterface.OnChangeToShutterSpeed;
                @ChangeToShutterSpeed.performed -= m_Wrapper.m_EquipmentsActionsCallbackInterface.OnChangeToShutterSpeed;
                @ChangeToShutterSpeed.canceled -= m_Wrapper.m_EquipmentsActionsCallbackInterface.OnChangeToShutterSpeed;
                @ChangeToAperture.started -= m_Wrapper.m_EquipmentsActionsCallbackInterface.OnChangeToAperture;
                @ChangeToAperture.performed -= m_Wrapper.m_EquipmentsActionsCallbackInterface.OnChangeToAperture;
                @ChangeToAperture.canceled -= m_Wrapper.m_EquipmentsActionsCallbackInterface.OnChangeToAperture;
                @ChangeToWhiteBalance.started -= m_Wrapper.m_EquipmentsActionsCallbackInterface.OnChangeToWhiteBalance;
                @ChangeToWhiteBalance.performed -= m_Wrapper.m_EquipmentsActionsCallbackInterface.OnChangeToWhiteBalance;
                @ChangeToWhiteBalance.canceled -= m_Wrapper.m_EquipmentsActionsCallbackInterface.OnChangeToWhiteBalance;
                @Increase.started -= m_Wrapper.m_EquipmentsActionsCallbackInterface.OnIncrease;
                @Increase.performed -= m_Wrapper.m_EquipmentsActionsCallbackInterface.OnIncrease;
                @Increase.canceled -= m_Wrapper.m_EquipmentsActionsCallbackInterface.OnIncrease;
                @Decrease.started -= m_Wrapper.m_EquipmentsActionsCallbackInterface.OnDecrease;
                @Decrease.performed -= m_Wrapper.m_EquipmentsActionsCallbackInterface.OnDecrease;
                @Decrease.canceled -= m_Wrapper.m_EquipmentsActionsCallbackInterface.OnDecrease;
                @ResetEquipment.started -= m_Wrapper.m_EquipmentsActionsCallbackInterface.OnResetEquipment;
                @ResetEquipment.performed -= m_Wrapper.m_EquipmentsActionsCallbackInterface.OnResetEquipment;
                @ResetEquipment.canceled -= m_Wrapper.m_EquipmentsActionsCallbackInterface.OnResetEquipment;
            }
            m_Wrapper.m_EquipmentsActionsCallbackInterface = instance;
            if (instance != null)
            {
                @ChangeToShutterSpeed.started += instance.OnChangeToShutterSpeed;
                @ChangeToShutterSpeed.performed += instance.OnChangeToShutterSpeed;
                @ChangeToShutterSpeed.canceled += instance.OnChangeToShutterSpeed;
                @ChangeToAperture.started += instance.OnChangeToAperture;
                @ChangeToAperture.performed += instance.OnChangeToAperture;
                @ChangeToAperture.canceled += instance.OnChangeToAperture;
                @ChangeToWhiteBalance.started += instance.OnChangeToWhiteBalance;
                @ChangeToWhiteBalance.performed += instance.OnChangeToWhiteBalance;
                @ChangeToWhiteBalance.canceled += instance.OnChangeToWhiteBalance;
                @Increase.started += instance.OnIncrease;
                @Increase.performed += instance.OnIncrease;
                @Increase.canceled += instance.OnIncrease;
                @Decrease.started += instance.OnDecrease;
                @Decrease.performed += instance.OnDecrease;
                @Decrease.canceled += instance.OnDecrease;
                @ResetEquipment.started += instance.OnResetEquipment;
                @ResetEquipment.performed += instance.OnResetEquipment;
                @ResetEquipment.canceled += instance.OnResetEquipment;
            }
        }
    }
    public EquipmentsActions @Equipments => new EquipmentsActions(this);
    public interface IPlayerControlActions
    {
        void OnFowardMovement(InputAction.CallbackContext context);
        void OnSidewayMovement(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
    }
    public interface IPlayerInteractionActions
    {
        void OnInteract(InputAction.CallbackContext context);
        void OnExitPuzzle(InputAction.CallbackContext context);
        void OnTurnRayLight(InputAction.CallbackContext context);
        void OnDialogueProceed(InputAction.CallbackContext context);
        void OnInteractItem(InputAction.CallbackContext context);
        void OnTestingButton(InputAction.CallbackContext context);
        void OnIncreaseIntensity(InputAction.CallbackContext context);
        void OnDecreaseIntensity(InputAction.CallbackContext context);
        void OnAutoCompletePuzzle(InputAction.CallbackContext context);
    }
    public interface IMouseActions
    {
        void OnClick(InputAction.CallbackContext context);
        void OnPosition(InputAction.CallbackContext context);
        void OnRightClick(InputAction.CallbackContext context);
        void OnDebugPosition(InputAction.CallbackContext context);
    }
    public interface IEquipmentsActions
    {
        void OnChangeToShutterSpeed(InputAction.CallbackContext context);
        void OnChangeToAperture(InputAction.CallbackContext context);
        void OnChangeToWhiteBalance(InputAction.CallbackContext context);
        void OnIncrease(InputAction.CallbackContext context);
        void OnDecrease(InputAction.CallbackContext context);
        void OnResetEquipment(InputAction.CallbackContext context);
    }
}
